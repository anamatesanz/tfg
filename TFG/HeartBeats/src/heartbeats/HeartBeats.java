package heartbeats;

import bitalinoPart.BITalino;
import bitalinoPart.BITalinoException;
import bitalinoPart.Frame;
import static bitalinoPart.BitalinoFunctionsInterface.*;
import static filter.LowPass.butterworthLowPassFilter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.io.File;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static stlPart.DoSTLFile.takeSTLFile;
import stlPart.STLViewer;
import static svgPart.DoSVGFile.*;
import static svgPart.SVGtoJPEG.changeToJPG;
import java.awt.Cursor;
import java.awt.FlowLayout;

public class HeartBeats extends javax.swing.JFrame {

    int MAX_WINDOWS = 1;
    private BITalino bitalino;
    private Vector myVectorECG;
    private Vector filteredVector;
    private Thread eTh;
    private String MacAdress;
    private Integer seconds;
    Integer channel;
    Rectangle captureRect;
    BufferedImage screen;
    File fileSTL;
    Integer samplingRate;
    Integer cutoff;
    List sub;
    Integer speed = 15;
    Integer counter;
    String type;

    public HeartBeats() {
        initComponents();
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("HeartBeats");
        MacAdress = macAdressText.getText();
        seconds = 8;
        samplingRate = 100;
        cutoff = 40;
        channel = 1;
        //setResizable(false);
        setExtendedState(MAXIMIZED_BOTH);
        counter = 0;
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HeartBeats();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleJPanle = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        contenedorPanel = new javax.swing.JPanel();
        jPanelSlider1 = new diu.swe.habib.JPanelSlider.JPanelSlider();
        settingsPanel = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel33 = new javax.swing.JPanel();
        connectBitalinoButton = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jPanel32 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel139 = new javax.swing.JPanel();
        jPanel140 = new javax.swing.JPanel();
        macAdressText = new javax.swing.JTextField();
        jPanel141 = new javax.swing.JPanel();
        jPanel142 = new javax.swing.JPanel();
        jPanel151 = new javax.swing.JPanel();
        jPanel152 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel153 = new javax.swing.JPanel();
        jPanel154 = new javax.swing.JPanel();
        channelComboBox = new javax.swing.JComboBox<>();
        jPanel155 = new javax.swing.JPanel();
        jPanel156 = new javax.swing.JPanel();
        jPanel157 = new javax.swing.JPanel();
        jPanel158 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel111 = new javax.swing.JPanel();
        jPanel134 = new javax.swing.JPanel();
        jSlider1 = new javax.swing.JSlider();
        numText = new javax.swing.JLabel();
        jPanel135 = new javax.swing.JPanel();
        jPanel136 = new javax.swing.JPanel();
        jPanel137 = new javax.swing.JPanel();
        jPanel138 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        samplingRateCombobox1 = new javax.swing.JComboBox<>();
        jPanel28 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        recordPanel = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jPanel40 = new javax.swing.JPanel();
        startButton = new javax.swing.JButton();
        jPanel44 = new javax.swing.JPanel();
        jPanel45 = new javax.swing.JPanel();
        jPanel46 = new javax.swing.JPanel();
        re_Record1 = new javax.swing.JButton();
        jPanel47 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jPanel37 = new javax.swing.JPanel();
        jPanel38 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jPanel39 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabelMac = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabelVersion = new javax.swing.JLabel();
        jPanel41 = new javax.swing.JPanel();
        jPanel42 = new javax.swing.JPanel();
        jPanel43 = new javax.swing.JPanel();
        loadingPanel = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel48 = new javax.swing.JPanel();
        jPanel49 = new javax.swing.JPanel();
        jPanel51 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel52 = new javax.swing.JPanel();
        jPanel53 = new javax.swing.JPanel();
        loadingLabel = new javax.swing.JLabel();
        jPanel59 = new javax.swing.JPanel();
        jPanel60 = new javax.swing.JPanel();
        jPanel61 = new javax.swing.JPanel();
        jPanel50 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jPanel54 = new javax.swing.JPanel();
        jPanel55 = new javax.swing.JPanel();
        jPanel56 = new javax.swing.JPanel();
        selectPartPanel = new javax.swing.JPanel();
        jPanel132 = new javax.swing.JPanel();
        jPanel133 = new javax.swing.JPanel();
        dataPanel = new javax.swing.JPanel();
        jPanel110 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jPanel29 = new javax.swing.JPanel();
        jPanel112 = new javax.swing.JPanel();
        jPanel113 = new javax.swing.JPanel();
        jPanel114 = new javax.swing.JPanel();
        lowPassButton = new javax.swing.JButton();
        jPanel115 = new javax.swing.JPanel();
        jPanel116 = new javax.swing.JPanel();
        jPanel117 = new javax.swing.JPanel();
        jPanel118 = new javax.swing.JPanel();
        jPanel119 = new javax.swing.JPanel();
        jPanel120 = new javax.swing.JPanel();
        jTextField1cutoffText = new javax.swing.JTextField();
        jPanel121 = new javax.swing.JPanel();
        jPanel122 = new javax.swing.JPanel();
        jPanel123 = new javax.swing.JPanel();
        jPanel124 = new javax.swing.JPanel();
        jPanel125 = new javax.swing.JPanel();
        jPanel146 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel147 = new javax.swing.JPanel();
        jPanel148 = new javax.swing.JPanel();
        jPanel149 = new javax.swing.JPanel();
        jPanel150 = new javax.swing.JPanel();
        jPanel170 = new javax.swing.JPanel();
        jPanel171 = new javax.swing.JPanel();
        jPanel143 = new javax.swing.JPanel();
        jPanel144 = new javax.swing.JPanel();
        jPanel145 = new javax.swing.JPanel();
        jPanel126 = new javax.swing.JPanel();
        jPanel127 = new javax.swing.JPanel();
        selectButton = new javax.swing.JButton();
        jPanel128 = new javax.swing.JPanel();
        jPanel129 = new javax.swing.JPanel();
        jPanel130 = new javax.swing.JPanel();
        re_Record = new javax.swing.JButton();
        jPanel131 = new javax.swing.JPanel();
        sizePanel = new javax.swing.JPanel();
        jPanel57 = new javax.swing.JPanel();
        jPanel58 = new javax.swing.JPanel();
        createSTL = new javax.swing.JButton();
        jPanel62 = new javax.swing.JPanel();
        jPanel63 = new javax.swing.JPanel();
        jPanel64 = new javax.swing.JPanel();
        re_Record2 = new javax.swing.JButton();
        returnButton = new javax.swing.JButton();
        jPanel65 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel66 = new javax.swing.JPanel();
        jPanel67 = new javax.swing.JPanel();
        jPanel35 = new javax.swing.JPanel();
        leftRingButton = new javax.swing.JToggleButton();
        upRingButton = new javax.swing.JToggleButton();
        rightRingButton = new javax.swing.JToggleButton();
        twoRingButton = new javax.swing.JToggleButton();
        jPanel36 = new javax.swing.JPanel();
        jPanel30 = new javax.swing.JPanel();
        jPanel69 = new javax.swing.JPanel();
        jPanel70 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel71 = new javax.swing.JPanel();
        jPanel72 = new javax.swing.JPanel();
        jPanel73 = new javax.swing.JPanel();
        jPanel80 = new javax.swing.JPanel();
        jPanel79 = new javax.swing.JPanel();
        jPanel81 = new javax.swing.JPanel();
        strokeHeightText = new javax.swing.JTextField();
        jPanel82 = new javax.swing.JPanel();
        jPanel83 = new javax.swing.JPanel();
        jPanel84 = new javax.swing.JPanel();
        jPanel85 = new javax.swing.JPanel();
        jPanel86 = new javax.swing.JPanel();
        jPanel87 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel88 = new javax.swing.JPanel();
        jPanel89 = new javax.swing.JPanel();
        jPanel90 = new javax.swing.JPanel();
        jPanel91 = new javax.swing.JPanel();
        jPanel92 = new javax.swing.JPanel();
        jPanel93 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel94 = new javax.swing.JPanel();
        jPanel95 = new javax.swing.JPanel();
        jPanel96 = new javax.swing.JPanel();
        jPanel97 = new javax.swing.JPanel();
        jPanel98 = new javax.swing.JPanel();
        jPanel99 = new javax.swing.JPanel();
        strokeWidthText = new javax.swing.JTextField();
        jPanel100 = new javax.swing.JPanel();
        jPanel101 = new javax.swing.JPanel();
        jPanel102 = new javax.swing.JPanel();
        jPanel103 = new javax.swing.JPanel();
        jPanel104 = new javax.swing.JPanel();
        jPanel105 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel106 = new javax.swing.JPanel();
        jPanel107 = new javax.swing.JPanel();
        jPanel108 = new javax.swing.JPanel();
        jPanel109 = new javax.swing.JPanel();
        jPanel68 = new javax.swing.JPanel();
        jPanel77 = new javax.swing.JPanel();
        jPanel78 = new javax.swing.JPanel();
        jPanel74 = new javax.swing.JPanel();
        jPanel75 = new javax.swing.JPanel();
        jPanel76 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(850, 570));
        setSize(new java.awt.Dimension(850, 570));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        titleJPanle.setBackground(new java.awt.Color(132, 188, 55));
        titleJPanle.setPreferredSize(new java.awt.Dimension(858, 100));

        jLabel1.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("HEART BEATS");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/logo2Resize.png"))); // NOI18N

        javax.swing.GroupLayout titleJPanleLayout = new javax.swing.GroupLayout(titleJPanle);
        titleJPanle.setLayout(titleJPanleLayout);
        titleJPanleLayout.setHorizontalGroup(
            titleJPanleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(titleJPanleLayout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        titleJPanleLayout.setVerticalGroup(
            titleJPanleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(titleJPanleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, titleJPanleLayout.createSequentialGroup()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(titleJPanle, java.awt.BorderLayout.PAGE_START);

        contenedorPanel.setBackground(new java.awt.Color(0, 0, 0));
        contenedorPanel.setLayout(new java.awt.BorderLayout());

        jPanelSlider1.setBackground(new java.awt.Color(255, 255, 255));

        settingsPanel.setBackground(new java.awt.Color(255, 255, 255));
        settingsPanel.setFocusTraversalPolicyProvider(true);
        settingsPanel.setMaximumSize(new java.awt.Dimension(858, 480));
        settingsPanel.setMinimumSize(new java.awt.Dimension(858, 480));
        settingsPanel.setPreferredSize(new java.awt.Dimension(858, 480));
        settingsPanel.setLayout(new java.awt.BorderLayout());

        jPanel16.setBackground(new java.awt.Color(132, 188, 55));
        jPanel16.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel16.setRequestFocusEnabled(false);
        jPanel16.setLayout(new java.awt.BorderLayout());

        jPanel33.setBackground(new java.awt.Color(132, 188, 55));
        jPanel33.setMaximumSize(new java.awt.Dimension(123, 33));
        jPanel33.setMinimumSize(new java.awt.Dimension(123, 33));
        jPanel33.setPreferredSize(new java.awt.Dimension(123, 33));

        connectBitalinoButton.setBackground(new java.awt.Color(0, 153, 0));
        connectBitalinoButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        connectBitalinoButton.setForeground(new java.awt.Color(255, 255, 255));
        connectBitalinoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Connected_30px.png"))); // NOI18N
        connectBitalinoButton.setText("CONNECT");
        connectBitalinoButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        connectBitalinoButton.setContentAreaFilled(false);
        connectBitalinoButton.setPreferredSize(new java.awt.Dimension(200, 50));
        connectBitalinoButton.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                connectBitalinoButtonMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                connectBitalinoButtonMouseMoved(evt);
            }
        });
        connectBitalinoButton.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                connectBitalinoButtonFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                connectBitalinoButtonFocusLost(evt);
            }
        });
        connectBitalinoButton.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                connectBitalinoButtonMouseWheelMoved(evt);
            }
        });
        connectBitalinoButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                connectBitalinoButtonMouseExited(evt);
            }
        });
        connectBitalinoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectBitalinoButtonActionPerformed(evt);
            }
        });
        jPanel33.add(connectBitalinoButton);

        jPanel16.add(jPanel33, java.awt.BorderLayout.CENTER);

        jPanel25.setBackground(new java.awt.Color(132, 188, 55));
        jPanel25.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel16.add(jPanel25, java.awt.BorderLayout.SOUTH);

        jPanel22.setBackground(new java.awt.Color(132, 188, 55));
        jPanel22.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel16.add(jPanel22, java.awt.BorderLayout.NORTH);

        jPanel32.setBackground(new java.awt.Color(132, 188, 55));
        jPanel32.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel16.add(jPanel32, java.awt.BorderLayout.EAST);

        jPanel31.setBackground(new java.awt.Color(132, 188, 55));
        jPanel31.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel16.add(jPanel31, java.awt.BorderLayout.WEST);

        settingsPanel.add(jPanel16, java.awt.BorderLayout.SOUTH);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(740, 340));
        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel17.setLayout(new javax.swing.BoxLayout(jPanel17, javax.swing.BoxLayout.X_AXIS));
        jPanel2.add(jPanel17, java.awt.BorderLayout.NORTH);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setPreferredSize(new java.awt.Dimension(760, 37));
        jPanel19.setLayout(new java.awt.GridLayout(0, 1));

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.setPreferredSize(new java.awt.Dimension(760, 37));
        jPanel20.setLayout(new java.awt.GridLayout(1, 0));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel8.setText("MAC Address:");
        jPanel20.add(jLabel8);

        jPanel139.setBackground(new java.awt.Color(255, 255, 255));
        jPanel139.setLayout(new java.awt.BorderLayout());

        jPanel140.setBackground(new java.awt.Color(255, 255, 255));
        jPanel140.setMinimumSize(new java.awt.Dimension(380, 31));

        macAdressText.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        macAdressText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        macAdressText.setText("20:16:02:14:75:66");
        macAdressText.setBorder(null);
        macAdressText.setMinimumSize(new java.awt.Dimension(200, 30));
        macAdressText.setPreferredSize(new java.awt.Dimension(200, 30));
        macAdressText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                macAdressTextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel140Layout = new javax.swing.GroupLayout(jPanel140);
        jPanel140.setLayout(jPanel140Layout);
        jPanel140Layout.setHorizontalGroup(
            jPanel140Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel140Layout.createSequentialGroup()
                .addComponent(macAdressText, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 130, Short.MAX_VALUE))
        );
        jPanel140Layout.setVerticalGroup(
            jPanel140Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel140Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(macAdressText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel139.add(jPanel140, java.awt.BorderLayout.CENTER);

        jPanel141.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel141Layout = new javax.swing.GroupLayout(jPanel141);
        jPanel141.setLayout(jPanel141Layout);
        jPanel141Layout.setHorizontalGroup(
            jPanel141Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel141Layout.setVerticalGroup(
            jPanel141Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 85, Short.MAX_VALUE)
        );

        jPanel139.add(jPanel141, java.awt.BorderLayout.WEST);

        jPanel142.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel142Layout = new javax.swing.GroupLayout(jPanel142);
        jPanel142.setLayout(jPanel142Layout);
        jPanel142Layout.setHorizontalGroup(
            jPanel142Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel142Layout.setVerticalGroup(
            jPanel142Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 85, Short.MAX_VALUE)
        );

        jPanel139.add(jPanel142, java.awt.BorderLayout.EAST);

        jPanel151.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel151Layout = new javax.swing.GroupLayout(jPanel151);
        jPanel151.setLayout(jPanel151Layout);
        jPanel151Layout.setHorizontalGroup(
            jPanel151Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel151Layout.setVerticalGroup(
            jPanel151Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel139.add(jPanel151, java.awt.BorderLayout.SOUTH);

        jPanel152.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel152Layout = new javax.swing.GroupLayout(jPanel152);
        jPanel152.setLayout(jPanel152Layout);
        jPanel152Layout.setHorizontalGroup(
            jPanel152Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel152Layout.setVerticalGroup(
            jPanel152Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel139.add(jPanel152, java.awt.BorderLayout.NORTH);

        jPanel20.add(jPanel139);

        jPanel19.add(jPanel20);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setMinimumSize(new java.awt.Dimension(670, 40));
        jPanel13.setLayout(new java.awt.GridLayout(1, 0));

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel11.setText("Bitalino Channel:");
        jPanel13.add(jLabel11);

        jPanel153.setBackground(new java.awt.Color(255, 255, 255));
        jPanel153.setLayout(new java.awt.BorderLayout());

        jPanel154.setBackground(new java.awt.Color(255, 255, 255));
        jPanel154.setMinimumSize(new java.awt.Dimension(380, 31));
        jPanel154.setPreferredSize(new java.awt.Dimension(380, 31));

        channelComboBox.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        channelComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "A1", "A2", "A3", "A4", "A5", "A6" }));
        channelComboBox.setSelectedIndex(1);
        channelComboBox.setToolTipText("\n");
        channelComboBox.setMinimumSize(new java.awt.Dimension(200, 30));
        channelComboBox.setPreferredSize(new java.awt.Dimension(200, 30));
        channelComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                channelComboBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel154Layout = new javax.swing.GroupLayout(jPanel154);
        jPanel154.setLayout(jPanel154Layout);
        jPanel154Layout.setHorizontalGroup(
            jPanel154Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel154Layout.createSequentialGroup()
                .addComponent(channelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 90, Short.MAX_VALUE))
        );
        jPanel154Layout.setVerticalGroup(
            jPanel154Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel154Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(channelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );

        jPanel153.add(jPanel154, java.awt.BorderLayout.CENTER);

        jPanel155.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel155Layout = new javax.swing.GroupLayout(jPanel155);
        jPanel155.setLayout(jPanel155Layout);
        jPanel155Layout.setHorizontalGroup(
            jPanel155Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel155Layout.setVerticalGroup(
            jPanel155Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel153.add(jPanel155, java.awt.BorderLayout.WEST);

        jPanel156.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel156Layout = new javax.swing.GroupLayout(jPanel156);
        jPanel156.setLayout(jPanel156Layout);
        jPanel156Layout.setHorizontalGroup(
            jPanel156Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel156Layout.setVerticalGroup(
            jPanel156Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel153.add(jPanel156, java.awt.BorderLayout.EAST);

        jPanel157.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel157Layout = new javax.swing.GroupLayout(jPanel157);
        jPanel157.setLayout(jPanel157Layout);
        jPanel157Layout.setHorizontalGroup(
            jPanel157Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel157Layout.setVerticalGroup(
            jPanel157Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel153.add(jPanel157, java.awt.BorderLayout.SOUTH);

        jPanel158.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel158Layout = new javax.swing.GroupLayout(jPanel158);
        jPanel158.setLayout(jPanel158Layout);
        jPanel158Layout.setHorizontalGroup(
            jPanel158Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel158Layout.setVerticalGroup(
            jPanel158Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel153.add(jPanel158, java.awt.BorderLayout.NORTH);

        jPanel13.add(jPanel153);

        jPanel19.add(jPanel13);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setPreferredSize(new java.awt.Dimension(760, 37));
        jPanel15.setLayout(new java.awt.GridLayout(1, 0));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel10.setText("Record time in seconds:");
        jPanel15.add(jLabel10);

        jPanel111.setBackground(new java.awt.Color(255, 255, 255));
        jPanel111.setLayout(new java.awt.BorderLayout());

        jPanel134.setBackground(new java.awt.Color(255, 255, 255));
        jPanel134.setMinimumSize(new java.awt.Dimension(380, 31));
        jPanel134.setPreferredSize(new java.awt.Dimension(380, 31));
        jPanel134.setLayout(new java.awt.GridLayout());

        jSlider1.setBackground(new java.awt.Color(255, 255, 255));
        jSlider1.setForeground(new java.awt.Color(40, 168, 225));
        jSlider1.setMaximum(15);
        jSlider1.setMinimum(8);
        jSlider1.setValue(8);
        jSlider1.setMinimumSize(new java.awt.Dimension(200, 30));
        jSlider1.setPreferredSize(new java.awt.Dimension(200, 30));
        jSlider1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider1StateChanged(evt);
            }
        });
        jPanel134.add(jSlider1);

        numText.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        numText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        numText.setText("8");
        jPanel134.add(numText);

        jPanel111.add(jPanel134, java.awt.BorderLayout.CENTER);

        jPanel135.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel135Layout = new javax.swing.GroupLayout(jPanel135);
        jPanel135.setLayout(jPanel135Layout);
        jPanel135Layout.setHorizontalGroup(
            jPanel135Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel135Layout.setVerticalGroup(
            jPanel135Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel111.add(jPanel135, java.awt.BorderLayout.WEST);

        jPanel136.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel136Layout = new javax.swing.GroupLayout(jPanel136);
        jPanel136.setLayout(jPanel136Layout);
        jPanel136Layout.setHorizontalGroup(
            jPanel136Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel136Layout.setVerticalGroup(
            jPanel136Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel111.add(jPanel136, java.awt.BorderLayout.EAST);

        jPanel137.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel137Layout = new javax.swing.GroupLayout(jPanel137);
        jPanel137.setLayout(jPanel137Layout);
        jPanel137Layout.setHorizontalGroup(
            jPanel137Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel137Layout.setVerticalGroup(
            jPanel137Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel111.add(jPanel137, java.awt.BorderLayout.SOUTH);

        jPanel138.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel138Layout = new javax.swing.GroupLayout(jPanel138);
        jPanel138.setLayout(jPanel138Layout);
        jPanel138Layout.setHorizontalGroup(
            jPanel138Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel138Layout.setVerticalGroup(
            jPanel138Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel111.add(jPanel138, java.awt.BorderLayout.NORTH);

        jPanel15.add(jPanel111);

        jPanel19.add(jPanel15);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(new java.awt.GridLayout(1, 0));

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel12.setText("Bitalino Sampling rate:");
        jPanel12.add(jLabel12);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setMinimumSize(new java.awt.Dimension(380, 31));

        samplingRateCombobox1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        samplingRateCombobox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100 Hz", "1000Hz" }));
        samplingRateCombobox1.setMinimumSize(new java.awt.Dimension(200, 30));
        samplingRateCombobox1.setPreferredSize(new java.awt.Dimension(200, 30));
        samplingRateCombobox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                samplingRateComboboxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 380, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(samplingRateCombobox1, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 90, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 85, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(samplingRateCombobox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel28.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel28, java.awt.BorderLayout.WEST);

        jPanel21.setBackground(new java.awt.Color(255, 255, 255));
        jPanel21.setPreferredSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel21, java.awt.BorderLayout.EAST);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setPreferredSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel14, java.awt.BorderLayout.SOUTH);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setPreferredSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel11, java.awt.BorderLayout.NORTH);

        jPanel12.add(jPanel1);

        jPanel19.add(jPanel12);

        jPanel2.add(jPanel19, java.awt.BorderLayout.CENTER);

        jPanel24.setBackground(new java.awt.Color(255, 255, 255));
        jPanel24.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel24.setLayout(new javax.swing.BoxLayout(jPanel24, javax.swing.BoxLayout.X_AXIS));
        jPanel2.add(jPanel24, java.awt.BorderLayout.WEST);

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel23.setLayout(new javax.swing.BoxLayout(jPanel23, javax.swing.BoxLayout.X_AXIS));
        jPanel2.add(jPanel23, java.awt.BorderLayout.EAST);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel18.setLayout(new javax.swing.BoxLayout(jPanel18, javax.swing.BoxLayout.X_AXIS));
        jPanel2.add(jPanel18, java.awt.BorderLayout.SOUTH);

        settingsPanel.add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanelSlider1.add(settingsPanel, "card2");

        recordPanel.setBackground(new java.awt.Color(255, 255, 255));
        recordPanel.setFocusTraversalPolicyProvider(true);
        recordPanel.setMaximumSize(new java.awt.Dimension(858, 480));
        recordPanel.setMinimumSize(new java.awt.Dimension(858, 480));
        recordPanel.setPreferredSize(new java.awt.Dimension(858, 480));
        recordPanel.setLayout(new java.awt.BorderLayout());

        jPanel27.setBackground(new java.awt.Color(132, 188, 55));
        jPanel27.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel27.setLayout(new java.awt.BorderLayout());

        jPanel40.setBackground(new java.awt.Color(132, 188, 55));
        jPanel40.setMaximumSize(new java.awt.Dimension(123, 33));
        jPanel40.setMinimumSize(new java.awt.Dimension(123, 33));
        jPanel40.setPreferredSize(new java.awt.Dimension(123, 33));

        startButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        startButton.setForeground(new java.awt.Color(255, 255, 255));
        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Record_30px.png"))); // NOI18N
        startButton.setText("RECORD");
        startButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        startButton.setContentAreaFilled(false);
        startButton.setPreferredSize(new java.awt.Dimension(200, 50));
        startButton.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                startButtonMouseMoved(evt);
            }
        });
        startButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                startButtonMouseExited(evt);
            }
        });
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });
        jPanel40.add(startButton);

        jPanel27.add(jPanel40, java.awt.BorderLayout.CENTER);

        jPanel44.setBackground(new java.awt.Color(132, 188, 55));
        jPanel44.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel44Layout = new javax.swing.GroupLayout(jPanel44);
        jPanel44.setLayout(jPanel44Layout);
        jPanel44Layout.setHorizontalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel44Layout.setVerticalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel27.add(jPanel44, java.awt.BorderLayout.SOUTH);

        jPanel45.setBackground(new java.awt.Color(132, 188, 55));
        jPanel45.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel45Layout = new javax.swing.GroupLayout(jPanel45);
        jPanel45.setLayout(jPanel45Layout);
        jPanel45Layout.setHorizontalGroup(
            jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel45Layout.setVerticalGroup(
            jPanel45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel27.add(jPanel45, java.awt.BorderLayout.NORTH);

        jPanel46.setBackground(new java.awt.Color(132, 188, 55));
        jPanel46.setPreferredSize(new java.awt.Dimension(100, 0));

        re_Record1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        re_Record1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Return_30px.png"))); // NOI18N
        re_Record1.setToolTipText("Record Again");
        re_Record1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        re_Record1.setContentAreaFilled(false);
        re_Record1.setPreferredSize(new java.awt.Dimension(100, 50));
        re_Record1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                re_Record1MouseMoved(evt);
            }
        });
        re_Record1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                re_Record1MouseExited(evt);
            }
        });
        re_Record1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                re_Record1ActionPerformed(evt);
            }
        });
        jPanel46.add(re_Record1);

        jPanel27.add(jPanel46, java.awt.BorderLayout.EAST);

        jPanel47.setBackground(new java.awt.Color(132, 188, 55));
        jPanel47.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel47Layout = new javax.swing.GroupLayout(jPanel47);
        jPanel47.setLayout(jPanel47Layout);
        jPanel47Layout.setHorizontalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel47Layout.setVerticalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel27.add(jPanel47, java.awt.BorderLayout.WEST);

        recordPanel.add(jPanel27, java.awt.BorderLayout.SOUTH);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setPreferredSize(new java.awt.Dimension(740, 340));
        jPanel7.setLayout(new java.awt.BorderLayout());

        jPanel34.setBackground(new java.awt.Color(255, 255, 255));
        jPanel34.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel34.setLayout(new javax.swing.BoxLayout(jPanel34, javax.swing.BoxLayout.X_AXIS));
        jPanel7.add(jPanel34, java.awt.BorderLayout.NORTH);

        jPanel37.setBackground(new java.awt.Color(255, 255, 255));
        jPanel37.setPreferredSize(new java.awt.Dimension(760, 37));
        jPanel37.setLayout(new javax.swing.BoxLayout(jPanel37, javax.swing.BoxLayout.Y_AXIS));

        jPanel38.setBackground(new java.awt.Color(255, 255, 255));
        jPanel38.setPreferredSize(new java.awt.Dimension(307, 180));
        jPanel38.setLayout(new javax.swing.BoxLayout(jPanel38, javax.swing.BoxLayout.X_AXIS));

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/bitalino.jpg"))); // NOI18N
        jPanel38.add(jLabel19);

        jPanel37.add(jPanel38);

        jPanel39.setBackground(new java.awt.Color(255, 255, 255));
        jPanel39.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel39.setLayout(new java.awt.GridLayout(2, 2));

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel22.setText("MAC Address:");
        jPanel39.add(jLabel22);

        jLabelMac.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jPanel39.add(jLabelMac);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel21.setText("Version:");
        jPanel39.add(jLabel21);

        jLabelVersion.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jPanel39.add(jLabelVersion);

        jPanel37.add(jPanel39);

        jPanel7.add(jPanel37, java.awt.BorderLayout.CENTER);

        jPanel41.setBackground(new java.awt.Color(255, 255, 255));
        jPanel41.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel41.setLayout(new javax.swing.BoxLayout(jPanel41, javax.swing.BoxLayout.X_AXIS));
        jPanel7.add(jPanel41, java.awt.BorderLayout.WEST);

        jPanel42.setBackground(new java.awt.Color(255, 255, 255));
        jPanel42.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel42.setLayout(new javax.swing.BoxLayout(jPanel42, javax.swing.BoxLayout.X_AXIS));
        jPanel7.add(jPanel42, java.awt.BorderLayout.EAST);

        jPanel43.setBackground(new java.awt.Color(255, 255, 255));
        jPanel43.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel43.setLayout(new javax.swing.BoxLayout(jPanel43, javax.swing.BoxLayout.X_AXIS));
        jPanel7.add(jPanel43, java.awt.BorderLayout.SOUTH);

        recordPanel.add(jPanel7, java.awt.BorderLayout.CENTER);

        jPanelSlider1.add(recordPanel, "card3");

        loadingPanel.setBackground(new java.awt.Color(255, 255, 255));
        loadingPanel.setFocusTraversalPolicyProvider(true);
        loadingPanel.setMaximumSize(new java.awt.Dimension(858, 480));
        loadingPanel.setMinimumSize(new java.awt.Dimension(858, 480));
        loadingPanel.setPreferredSize(new java.awt.Dimension(858, 480));
        loadingPanel.setLayout(new java.awt.BorderLayout());

        jPanel26.setBackground(new java.awt.Color(132, 188, 55));
        jPanel26.setPreferredSize(new java.awt.Dimension(858, 100));

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        loadingPanel.add(jPanel26, java.awt.BorderLayout.SOUTH);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setPreferredSize(new java.awt.Dimension(740, 340));
        jPanel8.setLayout(new java.awt.BorderLayout());

        jPanel48.setBackground(new java.awt.Color(255, 255, 255));
        jPanel48.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel48.setLayout(new javax.swing.BoxLayout(jPanel48, javax.swing.BoxLayout.X_AXIS));
        jPanel8.add(jPanel48, java.awt.BorderLayout.NORTH);

        jPanel49.setBackground(new java.awt.Color(255, 255, 255));
        jPanel49.setPreferredSize(new java.awt.Dimension(760, 37));
        jPanel49.setLayout(new java.awt.BorderLayout());

        jPanel51.setBackground(new java.awt.Color(255, 255, 255));
        jPanel51.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel51.setLayout(new java.awt.BorderLayout());

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setPreferredSize(new java.awt.Dimension(740, 340));
        jPanel9.setLayout(new java.awt.BorderLayout());

        jPanel52.setBackground(new java.awt.Color(255, 255, 255));
        jPanel52.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel52.setLayout(new javax.swing.BoxLayout(jPanel52, javax.swing.BoxLayout.X_AXIS));
        jPanel9.add(jPanel52, java.awt.BorderLayout.NORTH);

        jPanel53.setBackground(new java.awt.Color(255, 255, 255));
        jPanel53.setPreferredSize(new java.awt.Dimension(760, 37));

        loadingLabel.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        loadingLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/loadingGif.gif"))); // NOI18N
        jPanel53.add(loadingLabel);

        jPanel9.add(jPanel53, java.awt.BorderLayout.CENTER);

        jPanel59.setBackground(new java.awt.Color(255, 255, 255));
        jPanel59.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel59.setLayout(new javax.swing.BoxLayout(jPanel59, javax.swing.BoxLayout.X_AXIS));
        jPanel9.add(jPanel59, java.awt.BorderLayout.WEST);

        jPanel60.setBackground(new java.awt.Color(255, 255, 255));
        jPanel60.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel60.setLayout(new javax.swing.BoxLayout(jPanel60, javax.swing.BoxLayout.X_AXIS));
        jPanel9.add(jPanel60, java.awt.BorderLayout.EAST);

        jPanel61.setBackground(new java.awt.Color(255, 255, 255));
        jPanel61.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel61.setLayout(new javax.swing.BoxLayout(jPanel61, javax.swing.BoxLayout.X_AXIS));
        jPanel9.add(jPanel61, java.awt.BorderLayout.SOUTH);

        jPanel51.add(jPanel9, java.awt.BorderLayout.CENTER);

        jPanel49.add(jPanel51, java.awt.BorderLayout.CENTER);

        jPanel50.setBackground(new java.awt.Color(255, 255, 255));
        jPanel50.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel50.setLayout(new java.awt.GridLayout(1, 0));

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel18.setText("Recording data, please wait...");
        jLabel18.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jPanel50.add(jLabel18);

        jPanel49.add(jPanel50, java.awt.BorderLayout.NORTH);

        jPanel8.add(jPanel49, java.awt.BorderLayout.CENTER);

        jPanel54.setBackground(new java.awt.Color(255, 255, 255));
        jPanel54.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel54.setLayout(new javax.swing.BoxLayout(jPanel54, javax.swing.BoxLayout.X_AXIS));
        jPanel8.add(jPanel54, java.awt.BorderLayout.WEST);

        jPanel55.setBackground(new java.awt.Color(255, 255, 255));
        jPanel55.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel55.setLayout(new javax.swing.BoxLayout(jPanel55, javax.swing.BoxLayout.X_AXIS));
        jPanel8.add(jPanel55, java.awt.BorderLayout.EAST);

        jPanel56.setBackground(new java.awt.Color(255, 255, 255));
        jPanel56.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel56.setLayout(new javax.swing.BoxLayout(jPanel56, javax.swing.BoxLayout.X_AXIS));
        jPanel8.add(jPanel56, java.awt.BorderLayout.SOUTH);

        loadingPanel.add(jPanel8, java.awt.BorderLayout.CENTER);

        jPanelSlider1.add(loadingPanel, "card4");

        selectPartPanel.setBackground(new java.awt.Color(255, 255, 255));
        selectPartPanel.setFocusTraversalPolicyProvider(true);
        selectPartPanel.setMaximumSize(new java.awt.Dimension(858, 480));
        selectPartPanel.setMinimumSize(new java.awt.Dimension(858, 480));
        selectPartPanel.setPreferredSize(new java.awt.Dimension(858, 480));
        selectPartPanel.setLayout(new java.awt.BorderLayout());

        jPanel132.setBackground(new java.awt.Color(255, 255, 255));
        jPanel132.setPreferredSize(new java.awt.Dimension(740, 340));
        jPanel132.setLayout(new java.awt.BorderLayout());

        jPanel133.setBackground(new java.awt.Color(255, 255, 255));
        jPanel133.setPreferredSize(new java.awt.Dimension(0, 50));

        javax.swing.GroupLayout jPanel133Layout = new javax.swing.GroupLayout(jPanel133);
        jPanel133.setLayout(jPanel133Layout);
        jPanel133Layout.setHorizontalGroup(
            jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 740, Short.MAX_VALUE)
        );
        jPanel133Layout.setVerticalGroup(
            jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel132.add(jPanel133, java.awt.BorderLayout.NORTH);

        dataPanel.setBackground(new java.awt.Color(255, 255, 255));
        dataPanel.setPreferredSize(new java.awt.Dimension(760, 37));
        dataPanel.setLayout(new java.awt.BorderLayout());

        jPanel110.setBackground(new java.awt.Color(255, 255, 255));
        jPanel110.setPreferredSize(new java.awt.Dimension(760, 70));
        jPanel110.setLayout(new java.awt.GridLayout(1, 0));

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel24.setText("Select the interest part with the mouse:");
        jPanel110.add(jLabel24);

        jPanel29.setBackground(new java.awt.Color(132, 188, 55));
        jPanel29.setLayout(new java.awt.BorderLayout());

        jPanel112.setBackground(new java.awt.Color(132, 188, 55));
        jPanel112.setMinimumSize(new java.awt.Dimension(626, 222));
        jPanel112.setPreferredSize(new java.awt.Dimension(626, 222));
        jPanel112.setLayout(new java.awt.GridLayout(0, 3));

        jPanel113.setBackground(new java.awt.Color(132, 188, 55));
        jPanel113.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel113.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel113.setRequestFocusEnabled(false);
        jPanel113.setLayout(new java.awt.BorderLayout());

        jPanel114.setBackground(new java.awt.Color(132, 188, 55));
        jPanel114.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel114.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel114.setPreferredSize(new java.awt.Dimension(33, 33));

        lowPassButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lowPassButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Filter_64px.png"))); // NOI18N
        lowPassButton.setToolTipText("");
        lowPassButton.setAutoscrolls(true);
        lowPassButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        lowPassButton.setContentAreaFilled(false);
        lowPassButton.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                lowPassButtonMouseMoved(evt);
            }
        });
        lowPassButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lowPassButtonMouseExited(evt);
            }
        });
        lowPassButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lowPassButtonActionPerformed(evt);
            }
        });
        jPanel114.add(lowPassButton);

        jPanel113.add(jPanel114, java.awt.BorderLayout.CENTER);

        jPanel115.setBackground(new java.awt.Color(132, 188, 55));
        jPanel115.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel115Layout = new javax.swing.GroupLayout(jPanel115);
        jPanel115.setLayout(jPanel115Layout);
        jPanel115Layout.setHorizontalGroup(
            jPanel115Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel115Layout.setVerticalGroup(
            jPanel115Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel113.add(jPanel115, java.awt.BorderLayout.SOUTH);

        jPanel116.setBackground(new java.awt.Color(132, 188, 55));
        jPanel116.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel116Layout = new javax.swing.GroupLayout(jPanel116);
        jPanel116.setLayout(jPanel116Layout);
        jPanel116Layout.setHorizontalGroup(
            jPanel116Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel116Layout.setVerticalGroup(
            jPanel116Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel113.add(jPanel116, java.awt.BorderLayout.NORTH);

        jPanel117.setBackground(new java.awt.Color(132, 188, 55));
        jPanel117.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel117Layout = new javax.swing.GroupLayout(jPanel117);
        jPanel117.setLayout(jPanel117Layout);
        jPanel117Layout.setHorizontalGroup(
            jPanel117Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel117Layout.setVerticalGroup(
            jPanel117Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel113.add(jPanel117, java.awt.BorderLayout.EAST);

        jPanel118.setBackground(new java.awt.Color(132, 188, 55));
        jPanel118.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel118Layout = new javax.swing.GroupLayout(jPanel118);
        jPanel118.setLayout(jPanel118Layout);
        jPanel118Layout.setHorizontalGroup(
            jPanel118Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel118Layout.setVerticalGroup(
            jPanel118Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel113.add(jPanel118, java.awt.BorderLayout.WEST);

        jPanel112.add(jPanel113);

        jPanel119.setBackground(new java.awt.Color(132, 188, 55));
        jPanel119.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel119.setPreferredSize(new java.awt.Dimension(33, 33));
        jPanel119.setRequestFocusEnabled(false);
        jPanel119.setLayout(new java.awt.BorderLayout());

        jPanel120.setBackground(new java.awt.Color(132, 188, 55));
        jPanel120.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel120.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel120.setPreferredSize(new java.awt.Dimension(33, 33));

        jTextField1cutoffText.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jTextField1cutoffText.setText("40");
        jTextField1cutoffText.setPreferredSize(new java.awt.Dimension(33, 40));
        jTextField1cutoffText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1cutoffTextActionPerformed(evt);
            }
        });
        jPanel120.add(jTextField1cutoffText);

        jPanel119.add(jPanel120, java.awt.BorderLayout.CENTER);

        jPanel121.setBackground(new java.awt.Color(132, 188, 55));
        jPanel121.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel121Layout = new javax.swing.GroupLayout(jPanel121);
        jPanel121.setLayout(jPanel121Layout);
        jPanel121Layout.setHorizontalGroup(
            jPanel121Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel121Layout.setVerticalGroup(
            jPanel121Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel119.add(jPanel121, java.awt.BorderLayout.SOUTH);

        jPanel122.setBackground(new java.awt.Color(132, 188, 55));
        jPanel122.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel122Layout = new javax.swing.GroupLayout(jPanel122);
        jPanel122.setLayout(jPanel122Layout);
        jPanel122Layout.setHorizontalGroup(
            jPanel122Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel122Layout.setVerticalGroup(
            jPanel122Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel119.add(jPanel122, java.awt.BorderLayout.NORTH);

        jPanel123.setBackground(new java.awt.Color(132, 188, 55));
        jPanel123.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel123Layout = new javax.swing.GroupLayout(jPanel123);
        jPanel123.setLayout(jPanel123Layout);
        jPanel123Layout.setHorizontalGroup(
            jPanel123Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel123Layout.setVerticalGroup(
            jPanel123Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel119.add(jPanel123, java.awt.BorderLayout.EAST);

        jPanel124.setBackground(new java.awt.Color(132, 188, 55));
        jPanel124.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel124Layout = new javax.swing.GroupLayout(jPanel124);
        jPanel124.setLayout(jPanel124Layout);
        jPanel124Layout.setHorizontalGroup(
            jPanel124Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel124Layout.setVerticalGroup(
            jPanel124Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel119.add(jPanel124, java.awt.BorderLayout.WEST);

        jPanel112.add(jPanel119);

        jPanel125.setBackground(new java.awt.Color(132, 188, 55));
        jPanel125.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel125.setPreferredSize(new java.awt.Dimension(33, 33));
        jPanel125.setRequestFocusEnabled(false);
        jPanel125.setLayout(new java.awt.BorderLayout());

        jPanel146.setBackground(new java.awt.Color(132, 188, 55));
        jPanel146.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel146.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel146.setPreferredSize(new java.awt.Dimension(126, 25));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/info.png"))); // NOI18N
        jLabel2.setToolTipText("Apply a Low-Pass filter of the signal with the cutt-off frecuency in Hz from the text box");
        jPanel146.add(jLabel2);

        jPanel125.add(jPanel146, java.awt.BorderLayout.CENTER);

        jPanel147.setBackground(new java.awt.Color(132, 188, 55));
        jPanel147.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel147Layout = new javax.swing.GroupLayout(jPanel147);
        jPanel147.setLayout(jPanel147Layout);
        jPanel147Layout.setHorizontalGroup(
            jPanel147Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel147Layout.setVerticalGroup(
            jPanel147Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel125.add(jPanel147, java.awt.BorderLayout.SOUTH);

        jPanel148.setBackground(new java.awt.Color(132, 188, 55));
        jPanel148.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel148Layout = new javax.swing.GroupLayout(jPanel148);
        jPanel148.setLayout(jPanel148Layout);
        jPanel148Layout.setHorizontalGroup(
            jPanel148Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        jPanel148Layout.setVerticalGroup(
            jPanel148Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel125.add(jPanel148, java.awt.BorderLayout.NORTH);

        jPanel149.setBackground(new java.awt.Color(132, 188, 55));
        jPanel149.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel149Layout = new javax.swing.GroupLayout(jPanel149);
        jPanel149.setLayout(jPanel149Layout);
        jPanel149Layout.setHorizontalGroup(
            jPanel149Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel149Layout.setVerticalGroup(
            jPanel149Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel125.add(jPanel149, java.awt.BorderLayout.EAST);

        jPanel150.setBackground(new java.awt.Color(132, 188, 55));
        jPanel150.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel150Layout = new javax.swing.GroupLayout(jPanel150);
        jPanel150.setLayout(jPanel150Layout);
        jPanel150Layout.setHorizontalGroup(
            jPanel150Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel150Layout.setVerticalGroup(
            jPanel150Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel125.add(jPanel150, java.awt.BorderLayout.WEST);

        jPanel112.add(jPanel125);

        jPanel29.add(jPanel112, java.awt.BorderLayout.CENTER);

        jPanel170.setBackground(new java.awt.Color(255, 255, 255));
        jPanel170.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel170Layout = new javax.swing.GroupLayout(jPanel170);
        jPanel170.setLayout(jPanel170Layout);
        jPanel170Layout.setHorizontalGroup(
            jPanel170Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel170Layout.setVerticalGroup(
            jPanel170Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
        );

        jPanel29.add(jPanel170, java.awt.BorderLayout.WEST);

        jPanel171.setBackground(new java.awt.Color(255, 255, 255));
        jPanel171.setPreferredSize(new java.awt.Dimension(100, 0));
        jPanel171.setLayout(new javax.swing.BoxLayout(jPanel171, javax.swing.BoxLayout.X_AXIS));
        jPanel29.add(jPanel171, java.awt.BorderLayout.EAST);

        jPanel110.add(jPanel29);

        dataPanel.add(jPanel110, java.awt.BorderLayout.NORTH);

        jPanel132.add(dataPanel, java.awt.BorderLayout.CENTER);

        jPanel143.setBackground(new java.awt.Color(255, 255, 255));
        jPanel143.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel143.setLayout(new javax.swing.BoxLayout(jPanel143, javax.swing.BoxLayout.X_AXIS));
        jPanel132.add(jPanel143, java.awt.BorderLayout.WEST);

        jPanel144.setBackground(new java.awt.Color(255, 255, 255));
        jPanel144.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel144.setLayout(new javax.swing.BoxLayout(jPanel144, javax.swing.BoxLayout.X_AXIS));
        jPanel132.add(jPanel144, java.awt.BorderLayout.EAST);

        jPanel145.setBackground(new java.awt.Color(255, 255, 255));
        jPanel145.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel145.setLayout(new javax.swing.BoxLayout(jPanel145, javax.swing.BoxLayout.X_AXIS));
        jPanel132.add(jPanel145, java.awt.BorderLayout.SOUTH);

        selectPartPanel.add(jPanel132, java.awt.BorderLayout.CENTER);

        jPanel126.setBackground(new java.awt.Color(132, 188, 55));
        jPanel126.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel126.setRequestFocusEnabled(false);
        jPanel126.setLayout(new java.awt.BorderLayout());

        jPanel127.setBackground(new java.awt.Color(132, 188, 55));
        jPanel127.setMaximumSize(new java.awt.Dimension(123, 33));
        jPanel127.setMinimumSize(new java.awt.Dimension(123, 33));
        jPanel127.setPreferredSize(new java.awt.Dimension(123, 33));

        selectButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        selectButton.setForeground(new java.awt.Color(255, 255, 255));
        selectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/select.png"))); // NOI18N
        selectButton.setText("SELECT");
        selectButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        selectButton.setContentAreaFilled(false);
        selectButton.setPreferredSize(new java.awt.Dimension(200, 50));
        selectButton.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                selectButtonMouseMoved(evt);
            }
        });
        selectButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                selectButtonMouseExited(evt);
            }
        });
        selectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectButtonActionPerformed(evt);
            }
        });
        jPanel127.add(selectButton);

        jPanel126.add(jPanel127, java.awt.BorderLayout.CENTER);

        jPanel128.setBackground(new java.awt.Color(132, 188, 55));
        jPanel128.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel128Layout = new javax.swing.GroupLayout(jPanel128);
        jPanel128.setLayout(jPanel128Layout);
        jPanel128Layout.setHorizontalGroup(
            jPanel128Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel128Layout.setVerticalGroup(
            jPanel128Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel126.add(jPanel128, java.awt.BorderLayout.SOUTH);

        jPanel129.setBackground(new java.awt.Color(132, 188, 55));
        jPanel129.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel129Layout = new javax.swing.GroupLayout(jPanel129);
        jPanel129.setLayout(jPanel129Layout);
        jPanel129Layout.setHorizontalGroup(
            jPanel129Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel129Layout.setVerticalGroup(
            jPanel129Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel126.add(jPanel129, java.awt.BorderLayout.NORTH);

        jPanel130.setBackground(new java.awt.Color(132, 188, 55));
        jPanel130.setPreferredSize(new java.awt.Dimension(100, 0));

        re_Record.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        re_Record.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Reset_30px.png"))); // NOI18N
        re_Record.setToolTipText("Record Again");
        re_Record.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        re_Record.setContentAreaFilled(false);
        re_Record.setPreferredSize(new java.awt.Dimension(100, 50));
        re_Record.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                re_RecordMouseMoved(evt);
            }
        });
        re_Record.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                re_RecordMouseExited(evt);
            }
        });
        re_Record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                re_RecordActionPerformed(evt);
            }
        });
        jPanel130.add(re_Record);

        jPanel126.add(jPanel130, java.awt.BorderLayout.EAST);

        jPanel131.setBackground(new java.awt.Color(132, 188, 55));
        jPanel131.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel131Layout = new javax.swing.GroupLayout(jPanel131);
        jPanel131.setLayout(jPanel131Layout);
        jPanel131Layout.setHorizontalGroup(
            jPanel131Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel131Layout.setVerticalGroup(
            jPanel131Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel126.add(jPanel131, java.awt.BorderLayout.WEST);

        selectPartPanel.add(jPanel126, java.awt.BorderLayout.SOUTH);

        jPanelSlider1.add(selectPartPanel, "card5");

        sizePanel.setBackground(new java.awt.Color(255, 255, 255));
        sizePanel.setFocusTraversalPolicyProvider(true);
        sizePanel.setMaximumSize(new java.awt.Dimension(858, 480));
        sizePanel.setMinimumSize(new java.awt.Dimension(858, 480));
        sizePanel.setPreferredSize(new java.awt.Dimension(858, 480));
        sizePanel.setLayout(new java.awt.BorderLayout());

        jPanel57.setBackground(new java.awt.Color(132, 188, 55));
        jPanel57.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel57.setRequestFocusEnabled(false);
        jPanel57.setLayout(new java.awt.BorderLayout());

        jPanel58.setBackground(new java.awt.Color(132, 188, 55));
        jPanel58.setMaximumSize(new java.awt.Dimension(123, 33));
        jPanel58.setMinimumSize(new java.awt.Dimension(123, 33));
        jPanel58.setPreferredSize(new java.awt.Dimension(123, 33));

        createSTL.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        createSTL.setForeground(new java.awt.Color(255, 255, 255));
        createSTL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Add File_30px.png"))); // NOI18N
        createSTL.setText("CREATE STL");
        createSTL.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        createSTL.setContentAreaFilled(false);
        createSTL.setPreferredSize(new java.awt.Dimension(200, 50));
        createSTL.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                createSTLMouseMoved(evt);
            }
        });
        createSTL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                createSTLMouseExited(evt);
            }
        });
        createSTL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createSTLActionPerformed(evt);
            }
        });
        jPanel58.add(createSTL);

        jPanel57.add(jPanel58, java.awt.BorderLayout.CENTER);

        jPanel62.setBackground(new java.awt.Color(132, 188, 55));
        jPanel62.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel62Layout = new javax.swing.GroupLayout(jPanel62);
        jPanel62.setLayout(jPanel62Layout);
        jPanel62Layout.setHorizontalGroup(
            jPanel62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 858, Short.MAX_VALUE)
        );
        jPanel62Layout.setVerticalGroup(
            jPanel62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel57.add(jPanel62, java.awt.BorderLayout.SOUTH);

        jPanel63.setBackground(new java.awt.Color(132, 188, 55));
        jPanel63.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel63Layout = new javax.swing.GroupLayout(jPanel63);
        jPanel63.setLayout(jPanel63Layout);
        jPanel63Layout.setHorizontalGroup(
            jPanel63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 858, Short.MAX_VALUE)
        );
        jPanel63Layout.setVerticalGroup(
            jPanel63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel57.add(jPanel63, java.awt.BorderLayout.NORTH);

        jPanel64.setBackground(new java.awt.Color(132, 188, 55));
        jPanel64.setPreferredSize(new java.awt.Dimension(200, 0));

        re_Record2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        re_Record2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Reset_30px.png"))); // NOI18N
        re_Record2.setToolTipText("Record Again");
        re_Record2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        re_Record2.setContentAreaFilled(false);
        re_Record2.setPreferredSize(new java.awt.Dimension(50, 50));
        re_Record2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                re_Record2MouseMoved(evt);
            }
        });
        re_Record2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                re_Record2MouseExited(evt);
            }
        });
        re_Record2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                re_Record2ActionPerformed(evt);
            }
        });
        jPanel64.add(re_Record2);

        returnButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        returnButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Return_30px.png"))); // NOI18N
        returnButton.setToolTipText("");
        returnButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        returnButton.setContentAreaFilled(false);
        returnButton.setPreferredSize(new java.awt.Dimension(50, 50));
        returnButton.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                returnButtonMouseMoved(evt);
            }
        });
        returnButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                returnButtonMouseExited(evt);
            }
        });
        returnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnButtonActionPerformed(evt);
            }
        });
        jPanel64.add(returnButton);

        jPanel57.add(jPanel64, java.awt.BorderLayout.EAST);

        jPanel65.setBackground(new java.awt.Color(132, 188, 55));
        jPanel65.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel65Layout = new javax.swing.GroupLayout(jPanel65);
        jPanel65.setLayout(jPanel65Layout);
        jPanel65Layout.setHorizontalGroup(
            jPanel65Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel65Layout.setVerticalGroup(
            jPanel65Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel57.add(jPanel65, java.awt.BorderLayout.WEST);

        sizePanel.add(jPanel57, java.awt.BorderLayout.SOUTH);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setPreferredSize(new java.awt.Dimension(740, 340));
        jPanel10.setLayout(new java.awt.BorderLayout());

        jPanel66.setBackground(new java.awt.Color(255, 255, 255));
        jPanel66.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel66.setLayout(new javax.swing.BoxLayout(jPanel66, javax.swing.BoxLayout.X_AXIS));
        jPanel10.add(jPanel66, java.awt.BorderLayout.NORTH);

        jPanel67.setBackground(new java.awt.Color(255, 255, 255));
        jPanel67.setPreferredSize(new java.awt.Dimension(760, 37));
        jPanel67.setLayout(new java.awt.GridLayout(0, 1));

        jPanel35.setBackground(new java.awt.Color(255, 255, 255));
        jPanel35.setOpaque(false);
        jPanel35.setLayout(new java.awt.GridLayout(1, 0, 50, 0));

        leftRingButton.setBackground(new java.awt.Color(255, 255, 255));
        leftRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateleftResize.png"))); // NOI18N
        leftRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55)));
        leftRingButton.setContentAreaFilled(false);
        leftRingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leftRingButtonActionPerformed(evt);
            }
        });
        jPanel35.add(leftRingButton);

        upRingButton.setBackground(new java.awt.Color(255, 255, 255));
        upRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateupResize.png"))); // NOI18N
        upRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55)));
        upRingButton.setContentAreaFilled(false);
        upRingButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                upRingButtonStateChanged(evt);
            }
        });
        upRingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upRingButtonActionPerformed(evt);
            }
        });
        jPanel35.add(upRingButton);

        rightRingButton.setBackground(new java.awt.Color(255, 255, 255));
        rightRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartraterightResize.png"))); // NOI18N
        rightRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55)));
        rightRingButton.setContentAreaFilled(false);
        rightRingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rightRingButtonActionPerformed(evt);
            }
        });
        jPanel35.add(rightRingButton);

        twoRingButton.setBackground(new java.awt.Color(255, 255, 255));
        twoRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrate2Resize.png"))); // NOI18N
        twoRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55)));
        twoRingButton.setContentAreaFilled(false);
        twoRingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                twoRingButtonActionPerformed(evt);
            }
        });
        jPanel35.add(twoRingButton);

        jPanel67.add(jPanel35);

        jPanel36.setBackground(new java.awt.Color(255, 255, 255));
        jPanel36.setOpaque(false);
        jPanel36.setLayout(new java.awt.BorderLayout());

        jPanel30.setBackground(new java.awt.Color(132, 188, 55));
        jPanel30.setMinimumSize(new java.awt.Dimension(626, 230));
        jPanel30.setPreferredSize(new java.awt.Dimension(626, 250));
        jPanel30.setLayout(new java.awt.GridLayout(2, 3, 3, 3));

        jPanel69.setBackground(new java.awt.Color(132, 188, 55));
        jPanel69.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel69.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel69.setRequestFocusEnabled(false);
        jPanel69.setLayout(new java.awt.BorderLayout());

        jPanel70.setBackground(new java.awt.Color(132, 188, 55));
        jPanel70.setMaximumSize(new java.awt.Dimension(126, 30));
        jPanel70.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel70.setPreferredSize(new java.awt.Dimension(126, 30));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Stroke height:");
        jLabel6.setToolTipText("Customize the wide x hide of the ECG");
        jPanel70.add(jLabel6);

        jPanel69.add(jPanel70, java.awt.BorderLayout.CENTER);

        jPanel71.setBackground(new java.awt.Color(132, 188, 55));
        jPanel71.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel71Layout = new javax.swing.GroupLayout(jPanel71);
        jPanel71.setLayout(jPanel71Layout);
        jPanel71Layout.setHorizontalGroup(
            jPanel71Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel71Layout.setVerticalGroup(
            jPanel71Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel69.add(jPanel71, java.awt.BorderLayout.SOUTH);

        jPanel72.setBackground(new java.awt.Color(132, 188, 55));
        jPanel72.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel72Layout = new javax.swing.GroupLayout(jPanel72);
        jPanel72.setLayout(jPanel72Layout);
        jPanel72Layout.setHorizontalGroup(
            jPanel72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel72Layout.setVerticalGroup(
            jPanel72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel69.add(jPanel72, java.awt.BorderLayout.NORTH);

        jPanel73.setBackground(new java.awt.Color(132, 188, 55));
        jPanel73.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel73Layout = new javax.swing.GroupLayout(jPanel73);
        jPanel73.setLayout(jPanel73Layout);
        jPanel73Layout.setHorizontalGroup(
            jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel73Layout.setVerticalGroup(
            jPanel73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel69.add(jPanel73, java.awt.BorderLayout.EAST);

        jPanel80.setBackground(new java.awt.Color(132, 188, 55));
        jPanel80.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel80Layout = new javax.swing.GroupLayout(jPanel80);
        jPanel80.setLayout(jPanel80Layout);
        jPanel80Layout.setHorizontalGroup(
            jPanel80Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel80Layout.setVerticalGroup(
            jPanel80Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel69.add(jPanel80, java.awt.BorderLayout.WEST);

        jPanel30.add(jPanel69);

        jPanel79.setBackground(new java.awt.Color(132, 188, 55));
        jPanel79.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel79.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel79.setRequestFocusEnabled(false);
        jPanel79.setLayout(new java.awt.BorderLayout());

        jPanel81.setBackground(new java.awt.Color(132, 188, 55));
        jPanel81.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel81.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel81.setPreferredSize(new java.awt.Dimension(126, 25));

        strokeHeightText.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        strokeHeightText.setText("4");
        strokeHeightText.setPreferredSize(new java.awt.Dimension(59, 40));
        strokeHeightText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                strokeHeightTextActionPerformed(evt);
            }
        });
        jPanel81.add(strokeHeightText);

        jPanel79.add(jPanel81, java.awt.BorderLayout.CENTER);

        jPanel82.setBackground(new java.awt.Color(132, 188, 55));
        jPanel82.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel82Layout = new javax.swing.GroupLayout(jPanel82);
        jPanel82.setLayout(jPanel82Layout);
        jPanel82Layout.setHorizontalGroup(
            jPanel82Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel82Layout.setVerticalGroup(
            jPanel82Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel79.add(jPanel82, java.awt.BorderLayout.SOUTH);

        jPanel83.setBackground(new java.awt.Color(132, 188, 55));
        jPanel83.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel83Layout = new javax.swing.GroupLayout(jPanel83);
        jPanel83.setLayout(jPanel83Layout);
        jPanel83Layout.setHorizontalGroup(
            jPanel83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel83Layout.setVerticalGroup(
            jPanel83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel79.add(jPanel83, java.awt.BorderLayout.NORTH);

        jPanel84.setBackground(new java.awt.Color(132, 188, 55));
        jPanel84.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel84Layout = new javax.swing.GroupLayout(jPanel84);
        jPanel84.setLayout(jPanel84Layout);
        jPanel84Layout.setHorizontalGroup(
            jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel84Layout.setVerticalGroup(
            jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel79.add(jPanel84, java.awt.BorderLayout.EAST);

        jPanel85.setBackground(new java.awt.Color(132, 188, 55));
        jPanel85.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel85Layout = new javax.swing.GroupLayout(jPanel85);
        jPanel85.setLayout(jPanel85Layout);
        jPanel85Layout.setHorizontalGroup(
            jPanel85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel85Layout.setVerticalGroup(
            jPanel85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel79.add(jPanel85, java.awt.BorderLayout.WEST);

        jPanel30.add(jPanel79);

        jPanel86.setBackground(new java.awt.Color(132, 188, 55));
        jPanel86.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel86.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel86.setRequestFocusEnabled(false);
        jPanel86.setLayout(new java.awt.BorderLayout());

        jPanel87.setBackground(new java.awt.Color(132, 188, 55));
        jPanel87.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel87.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel87.setPreferredSize(new java.awt.Dimension(126, 25));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/info.png"))); // NOI18N
        jLabel3.setToolTipText("Customize the size of the height stroke of the lines of the ECG in milimiters");
        jPanel87.add(jLabel3);

        jPanel86.add(jPanel87, java.awt.BorderLayout.CENTER);

        jPanel88.setBackground(new java.awt.Color(132, 188, 55));
        jPanel88.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel88Layout = new javax.swing.GroupLayout(jPanel88);
        jPanel88.setLayout(jPanel88Layout);
        jPanel88Layout.setHorizontalGroup(
            jPanel88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel88Layout.setVerticalGroup(
            jPanel88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel86.add(jPanel88, java.awt.BorderLayout.SOUTH);

        jPanel89.setBackground(new java.awt.Color(132, 188, 55));
        jPanel89.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel89Layout = new javax.swing.GroupLayout(jPanel89);
        jPanel89.setLayout(jPanel89Layout);
        jPanel89Layout.setHorizontalGroup(
            jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel89Layout.setVerticalGroup(
            jPanel89Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel86.add(jPanel89, java.awt.BorderLayout.NORTH);

        jPanel90.setBackground(new java.awt.Color(132, 188, 55));
        jPanel90.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel90Layout = new javax.swing.GroupLayout(jPanel90);
        jPanel90.setLayout(jPanel90Layout);
        jPanel90Layout.setHorizontalGroup(
            jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel90Layout.setVerticalGroup(
            jPanel90Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel86.add(jPanel90, java.awt.BorderLayout.EAST);

        jPanel91.setBackground(new java.awt.Color(132, 188, 55));
        jPanel91.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel91Layout = new javax.swing.GroupLayout(jPanel91);
        jPanel91.setLayout(jPanel91Layout);
        jPanel91Layout.setHorizontalGroup(
            jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel91Layout.setVerticalGroup(
            jPanel91Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel86.add(jPanel91, java.awt.BorderLayout.WEST);

        jPanel30.add(jPanel86);

        jPanel92.setBackground(new java.awt.Color(132, 188, 55));
        jPanel92.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel92.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel92.setRequestFocusEnabled(false);
        jPanel92.setLayout(new java.awt.BorderLayout());

        jPanel93.setBackground(new java.awt.Color(132, 188, 55));
        jPanel93.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel93.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel93.setPreferredSize(new java.awt.Dimension(126, 25));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Stroke widht:");
        jLabel7.setToolTipText("Customize the wide x hide of the ECG");
        jPanel93.add(jLabel7);

        jPanel92.add(jPanel93, java.awt.BorderLayout.CENTER);

        jPanel94.setBackground(new java.awt.Color(132, 188, 55));
        jPanel94.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel94Layout = new javax.swing.GroupLayout(jPanel94);
        jPanel94.setLayout(jPanel94Layout);
        jPanel94Layout.setHorizontalGroup(
            jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel94Layout.setVerticalGroup(
            jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel92.add(jPanel94, java.awt.BorderLayout.SOUTH);

        jPanel95.setBackground(new java.awt.Color(132, 188, 55));
        jPanel95.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel95Layout = new javax.swing.GroupLayout(jPanel95);
        jPanel95.setLayout(jPanel95Layout);
        jPanel95Layout.setHorizontalGroup(
            jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel95Layout.setVerticalGroup(
            jPanel95Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel92.add(jPanel95, java.awt.BorderLayout.NORTH);

        jPanel96.setBackground(new java.awt.Color(132, 188, 55));
        jPanel96.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel96Layout = new javax.swing.GroupLayout(jPanel96);
        jPanel96.setLayout(jPanel96Layout);
        jPanel96Layout.setHorizontalGroup(
            jPanel96Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel96Layout.setVerticalGroup(
            jPanel96Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel92.add(jPanel96, java.awt.BorderLayout.EAST);

        jPanel97.setBackground(new java.awt.Color(132, 188, 55));
        jPanel97.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel97Layout = new javax.swing.GroupLayout(jPanel97);
        jPanel97.setLayout(jPanel97Layout);
        jPanel97Layout.setHorizontalGroup(
            jPanel97Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel97Layout.setVerticalGroup(
            jPanel97Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel92.add(jPanel97, java.awt.BorderLayout.WEST);

        jPanel30.add(jPanel92);

        jPanel98.setBackground(new java.awt.Color(132, 188, 55));
        jPanel98.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel98.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel98.setRequestFocusEnabled(false);
        jPanel98.setLayout(new java.awt.BorderLayout());

        jPanel99.setBackground(new java.awt.Color(132, 188, 55));
        jPanel99.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel99.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel99.setPreferredSize(new java.awt.Dimension(126, 25));

        strokeWidthText.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        strokeWidthText.setText("1");
        strokeWidthText.setPreferredSize(new java.awt.Dimension(59, 40));
        strokeWidthText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                strokeWidthTextActionPerformed(evt);
            }
        });
        jPanel99.add(strokeWidthText);

        jPanel98.add(jPanel99, java.awt.BorderLayout.CENTER);

        jPanel100.setBackground(new java.awt.Color(132, 188, 55));
        jPanel100.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel100Layout = new javax.swing.GroupLayout(jPanel100);
        jPanel100.setLayout(jPanel100Layout);
        jPanel100Layout.setHorizontalGroup(
            jPanel100Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel100Layout.setVerticalGroup(
            jPanel100Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel98.add(jPanel100, java.awt.BorderLayout.SOUTH);

        jPanel101.setBackground(new java.awt.Color(132, 188, 55));
        jPanel101.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel101Layout = new javax.swing.GroupLayout(jPanel101);
        jPanel101.setLayout(jPanel101Layout);
        jPanel101Layout.setHorizontalGroup(
            jPanel101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel101Layout.setVerticalGroup(
            jPanel101Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel98.add(jPanel101, java.awt.BorderLayout.NORTH);

        jPanel102.setBackground(new java.awt.Color(132, 188, 55));
        jPanel102.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel102Layout = new javax.swing.GroupLayout(jPanel102);
        jPanel102.setLayout(jPanel102Layout);
        jPanel102Layout.setHorizontalGroup(
            jPanel102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel102Layout.setVerticalGroup(
            jPanel102Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel98.add(jPanel102, java.awt.BorderLayout.EAST);

        jPanel103.setBackground(new java.awt.Color(132, 188, 55));
        jPanel103.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel103Layout = new javax.swing.GroupLayout(jPanel103);
        jPanel103.setLayout(jPanel103Layout);
        jPanel103Layout.setHorizontalGroup(
            jPanel103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel103Layout.setVerticalGroup(
            jPanel103Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel98.add(jPanel103, java.awt.BorderLayout.WEST);

        jPanel30.add(jPanel98);

        jPanel104.setBackground(new java.awt.Color(132, 188, 55));
        jPanel104.setMinimumSize(new java.awt.Dimension(126, 30));
        jPanel104.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel104.setRequestFocusEnabled(false);
        jPanel104.setLayout(new java.awt.BorderLayout());

        jPanel105.setBackground(new java.awt.Color(132, 188, 55));
        jPanel105.setMaximumSize(new java.awt.Dimension(126, 25));
        jPanel105.setMinimumSize(new java.awt.Dimension(126, 25));
        jPanel105.setPreferredSize(new java.awt.Dimension(126, 25));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/info.png"))); // NOI18N
        jLabel4.setToolTipText("Customize the size of the widht stroke lines of the ECG");
        jPanel105.add(jLabel4);
        jLabel4.getAccessibleContext().setAccessibleDescription("Customize the size of the widht stroke of the lines of the ECG");

        jPanel104.add(jPanel105, java.awt.BorderLayout.CENTER);

        jPanel106.setBackground(new java.awt.Color(132, 188, 55));
        jPanel106.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel106Layout = new javax.swing.GroupLayout(jPanel106);
        jPanel106.setLayout(jPanel106Layout);
        jPanel106Layout.setHorizontalGroup(
            jPanel106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel106Layout.setVerticalGroup(
            jPanel106Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel104.add(jPanel106, java.awt.BorderLayout.SOUTH);

        jPanel107.setBackground(new java.awt.Color(132, 188, 55));
        jPanel107.setPreferredSize(new java.awt.Dimension(0, 10));

        javax.swing.GroupLayout jPanel107Layout = new javax.swing.GroupLayout(jPanel107);
        jPanel107.setLayout(jPanel107Layout);
        jPanel107Layout.setHorizontalGroup(
            jPanel107Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
        );
        jPanel107Layout.setVerticalGroup(
            jPanel107Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );

        jPanel104.add(jPanel107, java.awt.BorderLayout.NORTH);

        jPanel108.setBackground(new java.awt.Color(132, 188, 55));
        jPanel108.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel108Layout = new javax.swing.GroupLayout(jPanel108);
        jPanel108.setLayout(jPanel108Layout);
        jPanel108Layout.setHorizontalGroup(
            jPanel108Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel108Layout.setVerticalGroup(
            jPanel108Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel104.add(jPanel108, java.awt.BorderLayout.EAST);

        jPanel109.setBackground(new java.awt.Color(132, 188, 55));
        jPanel109.setPreferredSize(new java.awt.Dimension(10, 0));

        javax.swing.GroupLayout jPanel109Layout = new javax.swing.GroupLayout(jPanel109);
        jPanel109.setLayout(jPanel109Layout);
        jPanel109Layout.setHorizontalGroup(
            jPanel109Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel109Layout.setVerticalGroup(
            jPanel109Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel104.add(jPanel109, java.awt.BorderLayout.WEST);

        jPanel30.add(jPanel104);

        jPanel36.add(jPanel30, java.awt.BorderLayout.CENTER);

        jPanel68.setBackground(new java.awt.Color(255, 255, 255));
        jPanel68.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel68.setLayout(new javax.swing.BoxLayout(jPanel68, javax.swing.BoxLayout.X_AXIS));
        jPanel36.add(jPanel68, java.awt.BorderLayout.NORTH);

        jPanel77.setBackground(new java.awt.Color(255, 255, 255));
        jPanel77.setPreferredSize(new java.awt.Dimension(100, 0));
        jPanel77.setLayout(new javax.swing.BoxLayout(jPanel77, javax.swing.BoxLayout.X_AXIS));
        jPanel36.add(jPanel77, java.awt.BorderLayout.WEST);

        jPanel78.setBackground(new java.awt.Color(255, 255, 255));
        jPanel78.setPreferredSize(new java.awt.Dimension(100, 0));
        jPanel78.setLayout(new javax.swing.BoxLayout(jPanel78, javax.swing.BoxLayout.X_AXIS));
        jPanel36.add(jPanel78, java.awt.BorderLayout.EAST);

        jPanel67.add(jPanel36);

        jPanel10.add(jPanel67, java.awt.BorderLayout.CENTER);

        jPanel74.setBackground(new java.awt.Color(255, 255, 255));
        jPanel74.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel74.setLayout(new javax.swing.BoxLayout(jPanel74, javax.swing.BoxLayout.X_AXIS));
        jPanel10.add(jPanel74, java.awt.BorderLayout.WEST);

        jPanel75.setBackground(new java.awt.Color(255, 255, 255));
        jPanel75.setPreferredSize(new java.awt.Dimension(50, 0));
        jPanel75.setLayout(new javax.swing.BoxLayout(jPanel75, javax.swing.BoxLayout.X_AXIS));
        jPanel10.add(jPanel75, java.awt.BorderLayout.EAST);

        jPanel76.setBackground(new java.awt.Color(255, 255, 255));
        jPanel76.setPreferredSize(new java.awt.Dimension(0, 50));
        jPanel76.setLayout(new javax.swing.BoxLayout(jPanel76, javax.swing.BoxLayout.X_AXIS));
        jPanel10.add(jPanel76, java.awt.BorderLayout.SOUTH);

        sizePanel.add(jPanel10, java.awt.BorderLayout.CENTER);

        jPanelSlider1.add(sizePanel, "card6");

        contenedorPanel.add(jPanelSlider1, java.awt.BorderLayout.CENTER);

        getContentPane().add(contenedorPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void selectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectButtonActionPerformed
        try {
            int[] points = selectStartAndStopPoints();
            int startPoint = points[0];
            int stopPoint = points[1];

            if (captureRect.getWidth() > 0 && captureRect.getHeight() > 0 & startPoint < stopPoint) {
                sub = filteredVector.subList(startPoint, stopPoint);
                jPanelSlider1.nextPanel(speed, sizePanel, false);
                leftRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 5));
                type = "Left";
                upRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateupResize.png")));
                upRingButton.setText(null);
                twoRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrate2Resize.png")));
                rightRingButton.setText(null);
                rightRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartraterightResize.png"))); // NOI18N
                leftRingButton.setText(null);
                leftRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateleftResize.png"))); // NOI18N

            } else {
                JOptionPane.showMessageDialog(null, "Please, select a part with thw mouse");
            }
        } catch (Exception ex) {

            JOptionPane.showMessageDialog(null, "Please, select a part with thw mouse");
        }
    }//GEN-LAST:event_selectButtonActionPerformed

    private void createSTLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createSTLActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        double anchur;
        double alture;
        try {
            alture = Double.parseDouble(strokeHeightText.getText());
            anchur = Double.parseDouble(strokeWidthText.getText());
        } catch (Exception ex) {
            Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "The value in the text boxes should be a number");
            anchur=0;
            alture = 0;}
        if (anchur > 5 | alture > 5) {
            JOptionPane.showMessageDialog(null, "Please, introduce a widht and a height numbers lower than 5");
        } else {
            if (anchur > 0 | alture > 0) {
                try {
                    fileSTL = takeSTLFile(sub, "../HeartBeats/emgSelectedSTL" 
                            + counter + ".stl", anchur, alture, type, samplingRate);
                    counter = counter + 1;
                } catch (IOException ex) {
                    Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);}
                new Thread(new stlViewerThread()).start();
            } else {
                JOptionPane.showMessageDialog(null, "The value in the text boxes should be bigger than 0");}
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_createSTLActionPerformed

    private void returnButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnButtonActionPerformed
        jPanelSlider1.nextPanel(speed, selectPartPanel, true);
    }//GEN-LAST:event_returnButtonActionPerformed

    private void lowPassButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lowPassButtonActionPerformed
        double[] array = new double[myVectorECG.size()];
        for (int i = 0; i < array.length; i++) {
            Integer num = (Integer) myVectorECG.get(i);
            array[i] = num.doubleValue();
        }
        try {
            cutoff = Integer.parseInt(jTextField1cutoffText.getText());
        } catch (Exception ex) {
            Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "The value in the text box should be a number");
            cutoff = null;
        }
        if (cutoff != null) {
            filteredVector = new Vector();
            double[] filtered = butterworthLowPassFilter(array, cutoff.doubleValue(), samplingRate.doubleValue());

            for (int i = 0; i < array.length; i++) {
                int num = (int) Math.round(filtered[i]);
                filteredVector.insertElementAt(num, i);
            }
        }
        BorderLayout layout = (BorderLayout) dataPanel.getLayout();
        jPanel34.remove(layout.getLayoutComponent(BorderLayout.CENTER));
        repaintWithData(filteredVector);
        dataPanel.updateUI();
    }//GEN-LAST:event_lowPassButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        jPanelSlider1.nextPanel(speed, loadingPanel, false);
        Runnable start = new taskInStartButton();
        Thread startThread = new Thread(start);
        startThread.start();

    }//GEN-LAST:event_startButtonActionPerformed

    private void jTextField1cutoffTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1cutoffTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1cutoffTextActionPerformed

    private void leftRingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leftRingButtonActionPerformed
        leftRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 5));
        rightRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        upRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        twoRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        type = "Left";
        leftRingButton.setIcon(null);
        leftRingButton.setText("Left Ring");
        upRingButton.setText(null);
        upRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateupResize.png"))); 
        rightRingButton.setText(null);
        rightRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartraterightResize.png"))); 
        twoRingButton.setText(null);
        twoRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrate2Resize.png")));
    }//GEN-LAST:event_leftRingButtonActionPerformed

    private void upRingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upRingButtonActionPerformed
        upRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 5));
        rightRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        leftRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        twoRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        type = "Up";
        upRingButton.setIcon(null);
        upRingButton.setText("Up Ring");
        twoRingButton.setText(null);
        twoRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrate2Resize.png")));
        rightRingButton.setText(null);
        rightRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartraterightResize.png"))); // NOI18N
        leftRingButton.setText(null);
        leftRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateleftResize.png"))); // NOI18N
    }//GEN-LAST:event_upRingButtonActionPerformed

    private void rightRingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rightRingButtonActionPerformed
        rightRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 5));
        upRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        leftRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        twoRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        rightRingButton.setIcon(null);
        rightRingButton.setText("Rigth Ring");
        upRingButton.setText(null);
        upRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateupResize.png"))); // NOI18N
        twoRingButton.setText(null);
        twoRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrate2Resize.png")));
        leftRingButton.setText(null);
        leftRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateleftResize.png"))); // NOI18N
        type = "Right";
    }//GEN-LAST:event_rightRingButtonActionPerformed

    private void twoRingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_twoRingButtonActionPerformed
        twoRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 5));
        rightRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        leftRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        upRingButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(132, 188, 55), 1));
        twoRingButton.setIcon(null);
        twoRingButton.setText("Two Ring");
        upRingButton.setText(null);
        upRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateupResize.png"))); // NOI18N
        rightRingButton.setText(null);
        rightRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartraterightResize.png"))); // NOI18N
        leftRingButton.setText(null);
        leftRingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/heartrateleftResize.png"))); // NOI18N
        type = "Two";
    }//GEN-LAST:event_twoRingButtonActionPerformed

    private void upRingButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_upRingButtonStateChanged

    }//GEN-LAST:event_upRingButtonStateChanged

    private void strokeHeightTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_strokeHeightTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_strokeHeightTextActionPerformed

    private void strokeWidthTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_strokeWidthTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_strokeWidthTextActionPerformed

    private void re_RecordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_re_RecordActionPerformed
        jPanelSlider1.nextPanel(speed, settingsPanel, true);
        captureRect = null;
    }//GEN-LAST:event_re_RecordActionPerformed

    private void re_Record1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_re_Record1ActionPerformed
        jPanelSlider1.nextPanel(speed, settingsPanel, true);
        try {
            disconnectBluethooth(bitalino);
        } catch (BITalinoException ex) {
            Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_re_Record1ActionPerformed

    private void re_Record2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_re_Record2ActionPerformed
        jPanelSlider1.nextPanel(speed, settingsPanel, true);
        captureRect = null;
    }//GEN-LAST:event_re_Record2ActionPerformed

    private void startButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_startButtonMouseExited
        startButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_startButtonMouseExited

    private void startButtonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_startButtonMouseMoved
        startButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));//Thinkness: 5
    }//GEN-LAST:event_startButtonMouseMoved

    private void re_Record1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re_Record1MouseExited
        re_Record1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_re_Record1MouseExited

    private void re_Record1MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re_Record1MouseMoved
        re_Record1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_re_Record1MouseMoved

    private void selectButtonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_selectButtonMouseMoved
        selectButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_selectButtonMouseMoved

    private void selectButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_selectButtonMouseExited
        selectButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_selectButtonMouseExited

    private void re_RecordMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re_RecordMouseMoved
        re_Record.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_re_RecordMouseMoved

    private void re_RecordMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re_RecordMouseExited
        re_Record.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_re_RecordMouseExited

    private void lowPassButtonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lowPassButtonMouseMoved
        lowPassButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_lowPassButtonMouseMoved

    private void lowPassButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lowPassButtonMouseExited
        lowPassButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_lowPassButtonMouseExited

    private void returnButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_returnButtonMouseExited
        returnButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_returnButtonMouseExited

    private void returnButtonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_returnButtonMouseMoved
        returnButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_returnButtonMouseMoved

    private void re_Record2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re_Record2MouseMoved
        re_Record2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_re_Record2MouseMoved

    private void re_Record2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re_Record2MouseExited
        re_Record2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_re_Record2MouseExited

    private void createSTLMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_createSTLMouseExited
        createSTL.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_createSTLMouseExited

    private void createSTLMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_createSTLMouseMoved
        createSTL.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_createSTLMouseMoved

    private void connectBitalinoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectBitalinoButtonActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if (macAdressText.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "The MAC is not a possible MAC Adress");
        } else {
            channel = channelComboBox.getSelectedIndex();
            MacAdress = macAdressText.getText();
            seconds = jSlider1.getValue();
            if (samplingRateCombobox1.getSelectedIndex() == 0) {
                samplingRate = 100;
            } else {
                samplingRate = 1000;}
            try {
                bitalino = connectBitalino(MacAdress, samplingRate);
            } catch (InterruptedException ex) {
                Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Its not possible to connect to the bitalino");
                bitalino = null;}
            if (bitalino != null) {
                jPanelSlider1.nextPanel(speed, recordPanel, false);
                try {
                    String ver = bitalino.version();
                    jLabelVersion.setText(ver.substring(0, 13));
                    jLabelMac.setText(MacAdress);
                } catch (Exception ex) {
                    Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);}}}
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_connectBitalinoButtonActionPerformed

    private void connectBitalinoButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_connectBitalinoButtonMouseExited
        connectBitalinoButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_connectBitalinoButtonMouseExited

    private void connectBitalinoButtonMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_connectBitalinoButtonMouseWheelMoved

    }//GEN-LAST:event_connectBitalinoButtonMouseWheelMoved

    private void connectBitalinoButtonFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_connectBitalinoButtonFocusLost

    }//GEN-LAST:event_connectBitalinoButtonFocusLost

    private void connectBitalinoButtonFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_connectBitalinoButtonFocusGained

    }//GEN-LAST:event_connectBitalinoButtonFocusGained

    private void connectBitalinoButtonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_connectBitalinoButtonMouseMoved
        connectBitalinoButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));//Thinkness: 5
    }//GEN-LAST:event_connectBitalinoButtonMouseMoved

    private void connectBitalinoButtonMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_connectBitalinoButtonMouseDragged

    }//GEN-LAST:event_connectBitalinoButtonMouseDragged

    private void samplingRateComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_samplingRateComboboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_samplingRateComboboxActionPerformed

    private void channelComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_channelComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_channelComboBoxActionPerformed

    private void jSlider1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider1StateChanged
        numText.setText(Integer.toString(jSlider1.getValue()));
    }//GEN-LAST:event_jSlider1StateChanged

    private void macAdressTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_macAdressTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_macAdressTextActionPerformed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        dataPanel.updateUI();
    }//GEN-LAST:event_formComponentResized


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> channelComboBox;
    private javax.swing.JButton connectBitalinoButton;
    private javax.swing.JPanel contenedorPanel;
    private javax.swing.JButton createSTL;
    private javax.swing.JPanel dataPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabelMac;
    private javax.swing.JLabel jLabelVersion;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel100;
    private javax.swing.JPanel jPanel101;
    private javax.swing.JPanel jPanel102;
    private javax.swing.JPanel jPanel103;
    private javax.swing.JPanel jPanel104;
    private javax.swing.JPanel jPanel105;
    private javax.swing.JPanel jPanel106;
    private javax.swing.JPanel jPanel107;
    private javax.swing.JPanel jPanel108;
    private javax.swing.JPanel jPanel109;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel110;
    private javax.swing.JPanel jPanel111;
    private javax.swing.JPanel jPanel112;
    private javax.swing.JPanel jPanel113;
    private javax.swing.JPanel jPanel114;
    private javax.swing.JPanel jPanel115;
    private javax.swing.JPanel jPanel116;
    private javax.swing.JPanel jPanel117;
    private javax.swing.JPanel jPanel118;
    private javax.swing.JPanel jPanel119;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel120;
    private javax.swing.JPanel jPanel121;
    private javax.swing.JPanel jPanel122;
    private javax.swing.JPanel jPanel123;
    private javax.swing.JPanel jPanel124;
    private javax.swing.JPanel jPanel125;
    private javax.swing.JPanel jPanel126;
    private javax.swing.JPanel jPanel127;
    private javax.swing.JPanel jPanel128;
    private javax.swing.JPanel jPanel129;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel130;
    private javax.swing.JPanel jPanel131;
    private javax.swing.JPanel jPanel132;
    private javax.swing.JPanel jPanel133;
    private javax.swing.JPanel jPanel134;
    private javax.swing.JPanel jPanel135;
    private javax.swing.JPanel jPanel136;
    private javax.swing.JPanel jPanel137;
    private javax.swing.JPanel jPanel138;
    private javax.swing.JPanel jPanel139;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel140;
    private javax.swing.JPanel jPanel141;
    private javax.swing.JPanel jPanel142;
    private javax.swing.JPanel jPanel143;
    private javax.swing.JPanel jPanel144;
    private javax.swing.JPanel jPanel145;
    private javax.swing.JPanel jPanel146;
    private javax.swing.JPanel jPanel147;
    private javax.swing.JPanel jPanel148;
    private javax.swing.JPanel jPanel149;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel150;
    private javax.swing.JPanel jPanel151;
    private javax.swing.JPanel jPanel152;
    private javax.swing.JPanel jPanel153;
    private javax.swing.JPanel jPanel154;
    private javax.swing.JPanel jPanel155;
    private javax.swing.JPanel jPanel156;
    private javax.swing.JPanel jPanel157;
    private javax.swing.JPanel jPanel158;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel170;
    private javax.swing.JPanel jPanel171;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel41;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel44;
    private javax.swing.JPanel jPanel45;
    private javax.swing.JPanel jPanel46;
    private javax.swing.JPanel jPanel47;
    private javax.swing.JPanel jPanel48;
    private javax.swing.JPanel jPanel49;
    private javax.swing.JPanel jPanel50;
    private javax.swing.JPanel jPanel51;
    private javax.swing.JPanel jPanel52;
    private javax.swing.JPanel jPanel53;
    private javax.swing.JPanel jPanel54;
    private javax.swing.JPanel jPanel55;
    private javax.swing.JPanel jPanel56;
    private javax.swing.JPanel jPanel57;
    private javax.swing.JPanel jPanel58;
    private javax.swing.JPanel jPanel59;
    private javax.swing.JPanel jPanel60;
    private javax.swing.JPanel jPanel61;
    private javax.swing.JPanel jPanel62;
    private javax.swing.JPanel jPanel63;
    private javax.swing.JPanel jPanel64;
    private javax.swing.JPanel jPanel65;
    private javax.swing.JPanel jPanel66;
    private javax.swing.JPanel jPanel67;
    private javax.swing.JPanel jPanel68;
    private javax.swing.JPanel jPanel69;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel70;
    private javax.swing.JPanel jPanel71;
    private javax.swing.JPanel jPanel72;
    private javax.swing.JPanel jPanel73;
    private javax.swing.JPanel jPanel74;
    private javax.swing.JPanel jPanel75;
    private javax.swing.JPanel jPanel76;
    private javax.swing.JPanel jPanel77;
    private javax.swing.JPanel jPanel78;
    private javax.swing.JPanel jPanel79;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel80;
    private javax.swing.JPanel jPanel81;
    private javax.swing.JPanel jPanel82;
    private javax.swing.JPanel jPanel83;
    private javax.swing.JPanel jPanel84;
    private javax.swing.JPanel jPanel85;
    private javax.swing.JPanel jPanel86;
    private javax.swing.JPanel jPanel87;
    private javax.swing.JPanel jPanel88;
    private javax.swing.JPanel jPanel89;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanel90;
    private javax.swing.JPanel jPanel91;
    private javax.swing.JPanel jPanel92;
    private javax.swing.JPanel jPanel93;
    private javax.swing.JPanel jPanel94;
    private javax.swing.JPanel jPanel95;
    private javax.swing.JPanel jPanel96;
    private javax.swing.JPanel jPanel97;
    private javax.swing.JPanel jPanel98;
    private javax.swing.JPanel jPanel99;
    private diu.swe.habib.JPanelSlider.JPanelSlider jPanelSlider1;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTextField jTextField1cutoffText;
    private javax.swing.JToggleButton leftRingButton;
    private javax.swing.JLabel loadingLabel;
    private javax.swing.JPanel loadingPanel;
    private javax.swing.JButton lowPassButton;
    private javax.swing.JTextField macAdressText;
    private javax.swing.JLabel numText;
    private javax.swing.JButton re_Record;
    private javax.swing.JButton re_Record1;
    private javax.swing.JButton re_Record2;
    private javax.swing.JPanel recordPanel;
    private javax.swing.JButton returnButton;
    private javax.swing.JToggleButton rightRingButton;
    private javax.swing.JComboBox<String> samplingRateCombobox1;
    private javax.swing.JButton selectButton;
    private javax.swing.JPanel selectPartPanel;
    private javax.swing.JPanel settingsPanel;
    private javax.swing.JPanel sizePanel;
    private javax.swing.JButton startButton;
    private javax.swing.JTextField strokeHeightText;
    private javax.swing.JTextField strokeWidthText;
    private javax.swing.JPanel titleJPanle;
    private javax.swing.JToggleButton twoRingButton;
    private javax.swing.JToggleButton upRingButton;
    // End of variables declaration//GEN-END:variables

    //Threads
    public class myBitalinoThread implements Runnable {
        BITalino bitali;
        public myBitalinoThread(BITalino bitalino) {
            this.bitali = bitalino;
        }
        @Override
        public void run() {
            Frame[] frameBit = null;
            int j = 0;
            int[] channelsToAcquire = {channel};
            try {
                this.bitali.start(channelsToAcquire);
            } catch (Throwable ex) {
                Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);}
            while (true) {
                try {
                    frameBit = this.bitali.read(1);
                } catch (BITalinoException ex) {
                    Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);}
                int i = frameBit[0].analog[0];
                myVectorECG.insertElementAt(i, j);
                j++;}}
    }

    public class taskInStartButton implements Runnable {
        public taskInStartButton() {
        }
        @Override
        public void run() {
            myVectorECG = new Vector();
            filteredVector = new Vector();
            Runnable e = new myBitalinoThread(bitalino);
            eTh = new Thread(e);
            eTh.start();

            try {
                sleep(seconds * 1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);}
            eTh.stop();
            try {
                disconnectBluethooth(bitalino);
            } catch (BITalinoException ex) {
                Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);}
            applyBitalinoTranferFunction(myVectorECG);
            repaintWithData(myVectorECG);
            filteredVector = myVectorECG;
            jPanelSlider1.nextPanel(speed, selectPartPanel, false);
        }
    }

    public class stlViewerThread implements Runnable {

        @Override
        public void run() {
            try {
                new STLViewer(fileSTL);
            } catch (IOException ex) {
                Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Interface functions
    //Selection part
    public void RectInJPEG(String path) throws IOException {
        screen = ImageIO.read(new File(path));
        BufferedImage screenCopy = new BufferedImage(screen.getWidth(), screen.getHeight(), screen.getType());
        JLabel screenLabel = new JLabel(new ImageIcon(screenCopy));
        JPanel newPanel = new JPanel(new FlowLayout());
        newPanel.add(screenLabel);
        newPanel.setBackground(Color.WHITE);
        JScrollPane scrollPanel = new JScrollPane(newPanel);
        repaint(screen, screenCopy);
        screenLabel.repaint();
        screenLabel.addMouseMotionListener(new MouseMotionAdapter() {
            Point start = new Point();
            public void mouseMoved(MouseEvent me) {
                start = me.getPoint();
                repaint(screen, screenCopy);
                screenLabel.repaint();}
            public void mouseDragged(MouseEvent me) {
                Point end = me.getPoint();
                Point esquinaSup = start;
                esquinaSup.y = 0;
                captureRect = new Rectangle(esquinaSup,
                        new Dimension(end.x - start.x, screenLabel.getHeight()));
                repaint(screen, screenCopy);
                screenLabel.repaint();}
        });
        dataPanel.add(scrollPanel, BorderLayout.CENTER);
    }

    public void repaint(BufferedImage orig, BufferedImage copy) {
        Graphics2D g = copy.createGraphics();
        g.drawImage(orig, 0, 0, null);
        if (captureRect != null) {
            g.setColor(Color.BLUE);
            g.draw(captureRect);
            g.setColor(new Color(128, 211, 253, 155));//red,green,blue,alpha
            g.fill(captureRect);
        }
        g.dispose();

    }

    private int[] selectStartAndStopPoints() {
        int initialPointRect = captureRect.x; //Punto inicial
        int maxAnchorRect = screen.getWidth(); //Punto maximo
        int maxPointRect = filteredVector.size();//Tamaño vector
        int anchorRect = captureRect.width;//Punto final
        int startPoint = (initialPointRect * maxPointRect) / maxAnchorRect; //x
        int stopPoint = ((initialPointRect + anchorRect) * maxPointRect) / maxAnchorRect;
        int[] points = new int[2];
        points[0] = startPoint;
        points[1] = stopPoint;
        return points;
    }

    public void repaintWithData(Vector vector) {
        File chartECG = null;
        String path = null;
        try {
            chartECG = takeSVGFile(vector.subList(0, vector.size()), samplingRate);

            path = changeToJPG(chartECG);

        } catch (Exception ex) {
            Logger.getLogger(HeartBeats.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        try {
            RectInJPEG(path);

        } catch (IOException ex1) {
            Logger.getLogger(HeartBeats.class
                    .getName()).log(Level.SEVERE, null, ex1);

        }

    }

    //SVG part
    public void applyBitalinoTranferFunction(Vector myVectorECG) {
        for (int i = 0; i < myVectorECG.size(); i++) {
            double value = ((((Integer) myVectorECG.get(i)) / 1024.) - 0.5);
            value = (value * 3.3) / 1100.;
            value = value * 1000.;
            value = value + 1.5; //value acoted between 0 and 3 mV
            if (samplingRate == 100) {
                value = value * 400; //anchor*large ECG value aplied and miliVolts to volts (value*0,4*1000)
            } else {
                value = value * 4000;
            }
            int intval = (int) Math.round(value);
            myVectorECG.setElementAt(intval, i);
        }
    }

}
