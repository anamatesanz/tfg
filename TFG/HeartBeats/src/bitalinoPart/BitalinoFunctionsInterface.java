package bitalinoPart;

import heartbeats.HeartBeats;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class BitalinoFunctionsInterface {

    public static BITalino connectBitalino(String MacAdress, int samplingRate) throws InterruptedException {

        BITalino bitalino = new BITalino();
        String macAddress = MacAdress;
        int SamplingRate = samplingRate;
        try {
            bitalino.open(macAddress, SamplingRate);
        } catch (BITalinoException ex) {
            Logger.getLogger(HeartBeats.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "It is not possible to connect to the bitalino.");
            return null;
        }
        return bitalino;
    }

    public static void disconnectBluethooth(BITalino bitalino) throws BITalinoException {
        if (bitalino != null) {
            bitalino.close();
        }
    }
}
