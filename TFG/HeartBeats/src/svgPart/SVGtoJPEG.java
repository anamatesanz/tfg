package svgPart;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;

public class SVGtoJPEG {

    public static String changeToJPG(File file) throws MalformedURLException, FileNotFoundException,
            TranscoderException, IOException {
        
        String svg_URI_input = file.toURL().toString();
        TranscoderInput input_svg_image = new TranscoderInput(svg_URI_input);
        String name = "emgPicture.jpg";
        OutputStream jpg_ostream = new FileOutputStream(name);
        TranscoderOutput output_jpg_image = new TranscoderOutput(jpg_ostream);
        JPEGTranscoder my_converter = new JPEGTranscoder();
        my_converter.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float(.9));
        my_converter.transcode(input_svg_image, output_jpg_image);
        jpg_ostream.flush();
        jpg_ostream.close();
        return name;
    }
}
