package svgPart;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;

public class DoSVGFile {

    public static void drawLines(Graphics2D g2, List vectorCanvas, Integer freq) {
        if (freq == 100) {
            for (int i = 0; i < vectorCanvas.size() - 1; i++) {
                g2.draw(new Line2D.Double(i, ((Integer) vectorCanvas.get(i)), (i + 1), ((Integer) vectorCanvas.get(i + 1))));
            }
        } else {
            for (int i = 0; i < vectorCanvas.size() - 1; i++) {
                g2.draw(new Line2D.Double(i / 10, ((Integer) vectorCanvas.get(i)) / 10, (i + 1) / 10, ((Integer) vectorCanvas.get(i + 1)) / 10));
            }
        }
    }

    public static File takeSVGFile(List sub, Integer freq) throws IOException {
        SVGGraphics2D g2 = new SVGGraphics2D(sub.size(), 1000);
        //g2.setBackground(Color.blue);
        drawLines(g2, sub, freq);
        File fileSVG = new File("emgSelectedSVG.svg");
        SVGUtils.writeToSVG(fileSVG, g2.getSVGElement());
        return fileSVG;
    }
}
