package stlPart;

import com.sun.j3d.utils.universe.SimpleUniverse;
import hall.collin.christopher.stl4j.STLParser;
import hall.collin.christopher.stl4j.Triangle;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import org.stlviewer.PCanvas3D;
import org.stlviewer.PModel;

public class STLViewer extends javax.swing.JFrame {

    PCanvas3D canvas;
    PModel model;
    SimpleUniverse universe;
    File fileSTL;

    public  STLViewer(File fileSTL) throws IOException {
        initComponents();
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("STL Viewer");
        addCanvas();
        this.fileSTL = fileSTL;
        loadFile();
        toFront();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chooseJDialog = new javax.swing.JDialog();
        chooseFileJFileChooser = new javax.swing.JFileChooser();
        principalPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel33 = new javax.swing.JPanel();
        saveAsButton = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jPanel32 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();

        chooseFileJFileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseFileJFileChooserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chooseJDialogLayout = new javax.swing.GroupLayout(chooseJDialog.getContentPane());
        chooseJDialog.getContentPane().setLayout(chooseJDialogLayout);
        chooseJDialogLayout.setHorizontalGroup(
            chooseJDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 623, Short.MAX_VALUE)
            .addGroup(chooseJDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(chooseJDialogLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(chooseFileJFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        chooseJDialogLayout.setVerticalGroup(
            chooseJDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 397, Short.MAX_VALUE)
            .addGroup(chooseJDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(chooseJDialogLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(chooseFileJFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1024, 768));

        principalPanel.setLayout(new java.awt.BorderLayout());
        getContentPane().add(principalPanel, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(132, 188, 55));
        jPanel2.setPreferredSize(new java.awt.Dimension(400, 25));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setPreferredSize(new java.awt.Dimension(858, 100));
        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel33.setBackground(new java.awt.Color(132, 188, 55));
        jPanel33.setMaximumSize(new java.awt.Dimension(123, 33));
        jPanel33.setMinimumSize(new java.awt.Dimension(123, 33));
        jPanel33.setPreferredSize(new java.awt.Dimension(123, 33));

        saveAsButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        saveAsButton.setForeground(new java.awt.Color(255, 255, 255));
        saveAsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Save_30px.png"))); // NOI18N
        saveAsButton.setText("Save as...");
        saveAsButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        saveAsButton.setContentAreaFilled(false);
        saveAsButton.setIconTextGap(5);
        saveAsButton.setPreferredSize(new java.awt.Dimension(200, 50));
        saveAsButton.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                saveAsButtonMouseMoved(evt);
            }
        });
        saveAsButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                saveAsButtonMouseExited(evt);
            }
        });
        saveAsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsButtonActionPerformed(evt);
            }
        });
        jPanel33.add(saveAsButton);

        jPanel3.add(jPanel33, java.awt.BorderLayout.CENTER);

        jPanel25.setBackground(new java.awt.Color(132, 188, 55));
        jPanel25.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel25, java.awt.BorderLayout.SOUTH);

        jPanel23.setBackground(new java.awt.Color(132, 188, 55));
        jPanel23.setPreferredSize(new java.awt.Dimension(0, 20));

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel23, java.awt.BorderLayout.NORTH);

        jPanel32.setBackground(new java.awt.Color(132, 188, 55));
        jPanel32.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel32, java.awt.BorderLayout.EAST);

        jPanel31.setBackground(new java.awt.Color(132, 188, 55));
        jPanel31.setPreferredSize(new java.awt.Dimension(100, 0));

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );

        jPanel3.add(jPanel31, java.awt.BorderLayout.WEST);

        getContentPane().add(jPanel3, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chooseFileJFileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseFileJFileChooserActionPerformed

    }//GEN-LAST:event_chooseFileJFileChooserActionPerformed

    private void saveAsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsButtonActionPerformed
        int seleccion = chooseFileJFileChooser.showSaveDialog(this);

        if (seleccion == JFileChooser.APPROVE_OPTION) {
            String fileSite = chooseFileJFileChooser.getSelectedFile().toString() + ".stl";
            fileSTL.renameTo(new File(fileSite));
        }

    }//GEN-LAST:event_saveAsButtonActionPerformed

    private void saveAsButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveAsButtonMouseExited
        saveAsButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 1));
    }//GEN-LAST:event_saveAsButtonMouseExited

    private void saveAsButtonMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveAsButtonMouseMoved
       saveAsButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 5));
    }//GEN-LAST:event_saveAsButtonMouseMoved

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser chooseFileJFileChooser;
    private javax.swing.JDialog chooseJDialog;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel principalPanel;
    private javax.swing.JButton saveAsButton;
    // End of variables declaration//GEN-END:variables

    private void addCanvas() {

        GraphicsConfiguration configuration = SimpleUniverse.getPreferredConfiguration();
        canvas = new PCanvas3D(configuration);
        principalPanel.add(canvas, BorderLayout.CENTER);
        universe = new SimpleUniverse(canvas);
        canvas.initcanvas(universe);
        canvas.setBackground(Color.CYAN);
    }

    public void loadFile() throws IOException {
        List<Triangle> mesh = new STLParser().parseSTLFile(fileSTL.toPath());
        if (model != null) {
            model.cleanup();
        }
        model = new PModel();
        model.addtriangles(mesh);
        canvas.rendermodel(model, universe);
    }
}
