package stlPart;

import static com.perunlabs.jsolid.JSolid.*;
import com.perunlabs.jsolid.d1.Angle;
import com.perunlabs.jsolid.d3.Solid;
import com.perunlabs.jsolid.d3.Vector3;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class DoSTLFile {

    static double height = 3;
    static double widht = 3;
    static double brazaletRadious = 10;

    public static Solid do3DLines(List vector, String type, Integer samplingRate) {
        Solid cilinderCustom = cylinder(0.1, 0.1);
        Solid bigBig = cilinderCustom;
        Solid bigSmall = bigBig;
        boolean comodin = false;
        for (int i = 1; i < vector.size(); i++) {
            Double x;
            Double value2 = new Double(((Integer) vector.get(i - 1)));
            Double value1 = new Double((Integer) vector.get(i));
            Double a = new Double(i);
            Double b = new Double(i - 1);
            x = Math.sqrt((Math.pow(Math.abs(value1 - value2), 2.) + Math.pow((a - b), 2.)));
            Solid smallBig = cuboid(x + widht, widht, height);
            Solid smallSmall = cuboid(x, widht, height);
            Double angle = Math.acos((a - b) / x);
            angle = Math.toDegrees(angle);

            if (value1 - value2 < 0) {
                angle = -1 * angle; //See if is in the first or the forth quadrante
            }
            
            Double radAngle = Math.toRadians(angle);
            Double distance = Math.tan(radAngle) * ((a - b) / 2.);
            distance = distance + value2;

            if (comodin == false) {
                comodin = true;
                bigBig = bigBig.add((smallBig).rotate(y(), degrees(angle)).moveBy(Vector3.vector3(i, 0, -distance)));
                bigSmall = bigSmall.add((smallSmall).rotate(y(), degrees(angle)).moveBy(Vector3.vector3(i, 0, -distance)));
            } else {

                bigSmall = bigSmall.add(smallSmall.rotate(y(), degrees(angle)).moveBy(Vector3.vector3(i, 0, -distance)));
                bigSmall = bigSmall.add(bigBig.intersect(smallBig.rotate(y(), degrees(angle)).moveBy(Vector3.vector3(i, 0, -distance))));
                bigBig = bigBig.add((smallBig).rotate(y(), degrees(angle)).moveBy(Vector3.vector3(i, 0, -distance)));
            }
        }
        bigSmall = bigSmall.sub(cilinderCustom);

        if (type.equals("Two")) {
            Solid ring = createRing();
            bigSmall = bigSmall.add(ring.moveBy(Vector3.vector3(-brazaletRadious + height, 0, -new Double(((Integer) vector.get(0))))));//x,y,z
            bigSmall = bigSmall.add(ring.moveBy(Vector3.vector3(new Double(((Integer) vector.size())) + brazaletRadious - height, 0, -new Double(((Integer) vector.get(vector.size() - 1))))));

        }
        if (type.equals("Right")) {
            Solid ring = createRing();
            bigSmall = bigSmall.add(ring.moveBy(Vector3.vector3(new Double(((Integer) vector.size())) + brazaletRadious - height, 0, -new Double(((Integer) vector.get(vector.size() - 1))))));
        }
        if (type.equals("Left")) {
            Solid ring = createRing();
            bigSmall = bigSmall.add(ring.moveBy(Vector3.vector3(-brazaletRadious + height, 0, -new Double(((Integer) vector.get(0))))));//x,y,z
        }
        if (type.equals("Up")) {
            Solid ring = createRing();
            Integer maxValue = 0;
            Integer position = 0;
            for (int i = 0; i < vector.size(); i++) {
                Integer value = (Integer) vector.get(i);
                if (value > maxValue) {
                    position = i;
                    maxValue = value;
                }
            }
            bigSmall = bigSmall.add(ring.moveBy(Vector3.vector3(new Double(position), 0, -maxValue - brazaletRadious - height)));

        }
        if (samplingRate == 1000) {
            bigSmall = scaleIn1000HzCase(bigSmall);
        }

        return bigSmall;
    }

    private static Solid rotateAndScale(Solid solid) {
        solid = solid.scale(x(), 0.5).scale(y(), 0.5).scale(z(), 0.5);
        Angle angle = degrees(90);
        solid = solid.rotate(x(), angle);
        return solid;
    }

    public static File save3DLines(Solid solid, String path) throws IOException {
        config().setCircleToPolygonPrecision(0.1);
        File emgSTL = new File(path);
        com.perunlabs.jsolid.d3.Stl.toStl(solid, emgSTL.toPath());
        return emgSTL;
    }

    private static Solid scaleIn1000HzCase(Solid solid) {
        solid = solid.scale(x(), 0.1);
        return solid;
    }

    private static Solid createRing() {
        Solid cil = cylinder(brazaletRadious, widht); //Radious and lenght
        Solid ring = cil.sub(cylinder(brazaletRadious - height, widht));
        Angle angle = degrees(90);
        ring = ring.rotate(x(), angle);
        return ring;
    }

    public static File takeSTLFile(List sub, String path, double anchor, double alture, String type, 
            Integer samplingRate) throws IOException {
        DoSTLFile.widht = alture * 2;
        DoSTLFile.height = anchor * 2;
        Solid solid = do3DLines(sub, type, samplingRate);
        solid = rotateAndScale(solid);
        File fileSTL = save3DLines(solid, path);
        return fileSTL;
    }

}
