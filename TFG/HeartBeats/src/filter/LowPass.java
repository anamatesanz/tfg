package filter;

import uk.me.berndporr.iirj.*;

public class LowPass {

    public static double[] butterworthLowPassFilter(double[] data, double cuttoff, double frequency) {
        Butterworth butterworth = new Butterworth();
        butterworth.lowPass(5, frequency, cuttoff);
        for (int i = 0; i < data.length; i++) {
            data[i] = butterworth.filter(data[i]);
        }
        return data;
    }
}
